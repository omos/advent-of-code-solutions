#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    coords = list(tuple(map(int, line.strip().split(', '))) for line in sys.stdin)

    x0 = min(c[0] for c in coords)
    y0 = min(c[1] for c in coords)
    x1 = max(c[0] for c in coords)
    y1 = max(c[1] for c in coords)

    areas = [0] * len(coords)
    excluded = set()
    for x in range(x0, x1 + 1):
        for y in range(y0, y1 + 1):
            min_dist = None
            min_coord = None
            only = None
            for i in range(len(coords)):
                xx = coords[i][0]
                yy = coords[i][1]
                dist = abs(xx - x) + abs(yy - y)
                if min_dist == None or dist < min_dist:
                    min_dist = dist
                    min_coord = i
                    only = True
                elif dist == min_dist:
                    only = False
            if only:
                areas[min_coord] += 1
                if x == x0 or x == x1 + 1 or y == y0 or y == y1 + 1:
                    excluded.add(min_coord)

    print(max(areas[i] for i in range(len(coords)) if i not in excluded))
