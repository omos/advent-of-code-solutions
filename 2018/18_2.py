#!/usr/bin/env python3

TARGET = 1000000000

if __name__ == '__main__':
    import sys
    from collections import defaultdict, Counter

    landscape = [line.strip() for line in sys.stdin]
    w = len(landscape[0])
    h = len(landscape)

    landscape = defaultdict('.'.strip,
                            (((r, c), landscape[r][c])
                             for r in range(h) for c in range(w)))
    hist = [landscape]
    rhist = { frozenset(landscape.items()): 0 }
    for i in range(1, TARGET + 1):
        new_landscape = defaultdict('.'.strip, landscape)
        for r in range(h):
            for c in range(w):
                cnt = Counter(landscape[(rr, cc)]
                              for rr in range(r - 1, r + 2)
                              for cc in range(c - 1, c + 2)
                              if rr != r or cc != c)
                if landscape[(r, c)] == '.':
                    if cnt['|'] >= 3:
                        new_landscape[(r, c)] = '|'
                elif landscape[(r, c)] == '|':
                    if cnt['#'] >= 3:
                        new_landscape[(r, c)] = '#'
                elif landscape[(r, c)] == '#':
                    if cnt['#'] == 0 or cnt['|'] == 0:
                        new_landscape[(r, c)] = '.'
        landscape = new_landscape
        k = frozenset(landscape.items())
        v = rhist.get(k, None)
        if v != None:
            landscape = hist[v + (TARGET - v) % (i - v)]
            break
        hist.append(landscape)
        rhist[k] = i

    cnt = Counter(landscape[(r, c)] for r in range(h) for c in range(w))
    print(cnt['|'] * cnt['#'])
