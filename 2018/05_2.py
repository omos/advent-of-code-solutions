#!/usr/bin/env python3

def react_length(s):
    polymer = list(s)
    i = 0
    while i < len(polymer) - 1:
        a = polymer[i]
        b = polymer[i + 1]
        if a.islower() != b.islower() and a.lower() == b.lower():
            del polymer[i : i + 2]
            if i > 0:
                i -= 1
        else:
            i += 1
    return len(polymer)

if __name__ == '__main__':
    import sys

    polymer = sys.stdin.read().strip()
    print(min(react_length(polymer.replace(c, '').replace(c.upper(), '')) for c in set(polymer.lower())))
