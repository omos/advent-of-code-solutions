#!/usr/bin/env python3

PAT_EPS = 2

if __name__ == '__main__':
    import sys

    initial = sys.stdin.readline().strip().split(': ')[1]
    sys.stdin.readline()

    state = frozenset(i for i in range(len(initial)) if initial[i] == '#')
    pats = set()
    for line in sys.stdin:
        pat, r = line.strip().split(' => ')
        if r == '#':
            pats.add(tuple(p == '#' for p in pat))

    for g in range(20):
        l = min(state)
        r = max(state)

        state = frozenset(i for i in range(l - PAT_EPS, r + PAT_EPS + 1) if tuple((i + k) in state for k in range(-PAT_EPS, PAT_EPS + 1)) in pats)

    print(sum(state))
