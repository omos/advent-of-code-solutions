#!/usr/bin/env python3

def parse_vec(text):
    text = text.split('=')[1]
    text = text.lstrip('<').rstrip('>')
    x, y = map(int, text.split(','))
    return x, y

def parse_point(text):
    return map(parse_vec, text.split('> '))

def print_message(points):
    x0 = min(p[0] for p in points)
    x1 = max(p[0] for p in points)
    y0 = min(p[1] for p in points)
    y1 = max(p[1] for p in points)
    points = frozenset(points)
    for y in range(y0, y1 + 1):
        print(''.join('#' if (x, y) in points else '.' for x in range(x0, x1 + 1)))

if __name__ == '__main__':
    import sys

    points = []
    speeds = []
    n = 0
    for l in sys.stdin:
        point, speed = parse_point(l.strip())
        points.append(point)
        speeds.append(speed)
        n += 1

    t = 0
    size = None
    while True:
        x0 = min(p[0] for p in points)
        x1 = max(p[0] for p in points)
        y0 = min(p[1] for p in points)
        y1 = max(p[1] for p in points)
        prev_size = size
        size = max(x1 - x0, y1 - y0)
        if prev_size != None and size > prev_size:
            break
        prev_points = list(points)
        for i in range(n):
            points[i] = (points[i][0] + speeds[i][0], points[i][1] + speeds[i][1])
        t += 1
    print(t - 1)
