#!/usr/bin/env python3

def dist(p1, p2):
    return sum(abs(p1[i] - p2[i]) for i in range(len(p1)))

if __name__ == '__main__':
    import sys

    bots = []
    for line in sys.stdin:
        pos, r = map(lambda s: s.split('=')[1], line.rstrip().split(', '))
        pos = tuple(map(int, pos.lstrip('<').rstrip('>').split(',')))
        r = int(r)
        bots.append((pos, r))

    max_pos, max_r = max(bots, key=lambda x: x[1])

    print(sum(1 for pos, r in bots if dist(pos, max_pos) <= max_r))
