#!/usr/bin/env python3

def compute_level(s, x, y):
    rid = x + 10
    l = rid * (rid * y + s)
    l //= 100
    l %= 10
    return l - 5

if __name__ == '__main__':
    SERIAL = 6042

    power_levels = [[compute_level(SERIAL, x, y) for y in range(300)] for x in range(300)]

    best_pos = None
    best_power = 0
    for x in range(300 - 3):
        for y in range(300 - 3):
            power = sum(power_levels[x + dx][y + dy] for dx in range(3) for dy in range(3))
            if best_power == None or power > best_power:
                best_pos = (x, y)
                best_power = power
                if best_power == 4 * 9:
                    break
        if best_power == 4 * 9:
            break
    print(','.join(map(str, best_pos)))
