#!/usr/bin/env python3

def get_duration(step):
    return 61 + ord(step) - ord('A')

def run_workers(deps, n):
    todo = list(deps)
    todo.sort()
    not_done = set(deps)
    workers = []
    t = 0
    while len(not_done):
        again = True
        while again and len(workers) < n:
            again = False
            for i in range(len(todo)):
                if all(d not in not_done for d in deps[todo[i]]):
                    workers.append((todo[i], t + get_duration(todo[i])))
                    del todo[i]
                    again = True
                    break
        t = min(w[1] for w in workers)
        i = 0
        while i < len(workers):
            if workers[i][1] == t:
                not_done.remove(workers[i][0])
                del workers[i]
            else:
                i += 1
    return t

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    deps = defaultdict(list)
    for req in sys.stdin:
        words = req.strip().split(' ')
        p = words[7]
        c = words[1]
        deps[p].append(c)
        deps[c].extend([])

    print(run_workers(deps, 5))
