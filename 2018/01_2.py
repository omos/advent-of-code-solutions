#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    deltas = [int(line.strip()) for line in sys.stdin]
    f = 0
    hist = set()
    while True:
        for d in deltas:
            f += d
            if f in hist:
                print(f)
                sys.exit(0)
            hist.add(f)
