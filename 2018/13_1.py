#!/usr/bin/env python3

DIR_W = 0
DIR_E = 1
DIR_N = 2
DIR_S = 3

DIR_TO_VEC = [(-1, 0), (1, 0), (0, -1), (0, 1)]
CART_TO_DIR = { '<': DIR_W, '>': DIR_E, '^': DIR_N, 'v': DIR_S }
CART_TO_TRACK = { '<': '-', '>': '-', '^': '|', 'v': '|'}
TRANS = {
        '|':  [None, None, [DIR_N], [DIR_S]],
        '-':  [[DIR_W], [DIR_E], None, None],
        '/':  [[DIR_S], [DIR_N], [DIR_E], [DIR_W]],
        '\\': [[DIR_N], [DIR_S], [DIR_W], [DIR_E]],
        '+':  [
                [DIR_S, DIR_W, DIR_N],
                [DIR_N, DIR_E, DIR_S],
                [DIR_W, DIR_N, DIR_E],
                [DIR_E, DIR_S, DIR_W],
            ],
    }

if __name__ == '__main__':
    import sys

    track = {}
    carts = {}
    width = 0
    y = 0
    for line in sys.stdin:
        line = line.rstrip('\n')
        for x in range(len(line)):
            if line[x] in '|-/\\+':
                track[(x, y)] = line[x]
            elif line[x] in '<>^v':
                track[(x, y)] = CART_TO_TRACK[line[x]]
                carts[(x, y)] = (CART_TO_DIR[line[x]], 0)
            else:
                assert line[x] == ' '
        width = max(width, len(line))
        y += 1

    height = y

    while True:
        for x, y in sorted(carts, key=lambda p: p[1] * width + p[0]):
            direction, counter = carts[(x, y)]
            dx, dy = DIR_TO_VEC[direction]
            del carts[(x, y)]
            x += dx
            y += dy
            if (x, y) in carts:
                print('{0},{1}'.format(x, y))
                sys.exit(0)
            options = TRANS[track[(x, y)]][direction]
            if options == None:
                print((x, y), direction, track[(x, y)])
            direction = options[counter % len(options)]
            if len(options) > 1:
                counter += 1
            carts[(x, y)] = (direction, counter)
