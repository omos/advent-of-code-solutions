#!/usr/bin/env python3

def parse_regs(line):
    return tuple(map(int, line.split(':')[1].lstrip(' [').rstrip(']').split(', ')))

def op_add(a, b):
    return a + b

def op_mul(a, b):
    return a * b

def op_ban(a, b):
    return a & b

def op_bor(a, b):
    return a | b

def op_set(a, b):
    return a

def op_gt(a, b):
    return 1 if a > b else 0

def op_eq(a, b):
    return 1 if a == b else 0

OPCODES = [
        (op_add, 0, 1),
        (op_add, 0, 0),
        (op_mul, 0, 1),
        (op_mul, 0, 0),
        (op_ban, 0, 1),
        (op_ban, 0, 0),
        (op_bor, 0, 1),
        (op_bor, 0, 0),
        (op_set, 1, 1),
        (op_set, 0, 1),
        (op_gt, 0, 0),
        (op_gt, 0, 1),
        (op_gt, 1, 0),
        (op_eq, 0, 0),
        (op_eq, 0, 1),
        (op_eq, 1, 0),
    ]

def check_ins(op, va, vb, ins, r0, r1):
    a = ins[1] if va else r0[ins[1]]
    b = ins[2] if vb else r0[ins[2]]
    return op(a, b) == r1[ins[3]]

if __name__ == '__main__':
    import sys

    total = 0
    while True:
        before = sys.stdin.readline().strip()
        if not len(before):
            break

        r0 = parse_regs(before)
        ins = list(map(int, sys.stdin.readline().strip().split(' ')))
        r1 = parse_regs(sys.stdin.readline().strip())
        sys.stdin.readline()

        total += sum(check_ins(op, va, vb, ins, r0, r1) for op, va, vb in OPCODES) >= 3

    print(total)
