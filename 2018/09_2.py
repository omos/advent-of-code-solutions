#!/usr/bin/env python3

def ll_init(v):
    node = [v, None, None]
    node[1] = node[2] = node
    return [node]

def ll_left(l):
    l[0] = l[0][1]

def ll_right(l):
    l[0] = l[0][2]

def ll_ins(l, v):
    node = [v, l[0][1], l[0]]
    l[0][1][2] = node
    l[0][1] = node
    l[0] = node

def ll_del(l):
    v, p, n = l[0]
    p[2] = n
    n[1] = p
    l[0] = n
    return v

def ll_debug(l):
    l = s = l[0]
    c = s[2]
    yield s[0]
    while c != s:
        yield c[0]
        assert c[1][2] == c
        assert c[1] == l
        assert l[2] == c
        l = c
        c = c[2]

def get_highscore(players, last):
    scores = [0] * players
    circle = ll_init(0)
    player = 0
    for m in range(1, last + 1):
        #if m % 100000 == 0:
            #print(m)
        if m % 23 != 0:
            ll_right(circle)
            ll_right(circle)
            #print('>', ' '.join(map(str, ll_debug(circle))))
            ll_ins(circle, m)
        else:
            scores[player] += m
            for i in range(7):
                ll_left(circle)
            scores[player] += ll_del(circle)
        #print(' '.join(map(str, ll_debug(circle))))
        player += 1
        player %= players
    return max(scores)

if __name__ == '__main__':
    import sys

    assert get_highscore(9, 25) == 32
    assert get_highscore(10, 1618) == 8317
    assert get_highscore(13, 7999) == 146373
    assert get_highscore(17, 1104) == 2764
    assert get_highscore(21, 6111) == 54718
    assert get_highscore(30, 5807) == 37305
    #print(get_highscore(476, 71657))
    print(get_highscore(476, 7165700))
