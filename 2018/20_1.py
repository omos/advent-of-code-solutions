#!/usr/bin/env python3

DIR_TO_DX = { 'N': 0, 'S':  0, 'E': 1, 'W': -1 }
DIR_TO_DY = { 'N': 1, 'S': -1, 'E': 0, 'W':  0 }

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    regex = sys.stdin.read().rstrip('$\n').lstrip('^')
    distmap = {}
    stack = []
    steps = 0
    x, y = 0, 0
    for c in regex:
        if c in 'NSEW':
            x += DIR_TO_DX[c]
            y += DIR_TO_DY[c]
            steps += 1
            if (x, y) not in distmap or steps < distmap[(x, y)]:
                distmap[(x, y)] = steps
        elif c == '(':
            stack.append((steps, x, y))
        elif c == ')':
            steps, x, y = stack.pop()
        elif c == '|':
            steps, x, y = stack[-1]

    print(max(distmap.values()))
