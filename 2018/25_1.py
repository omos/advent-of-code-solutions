#!/usr/bin/env python3

def dist(p1, p2):
    return sum(abs(p1[i] - p2[i]) for i in range(len(p1)))

if __name__ == '__main__':
    import sys

    points = [tuple(map(int, line.rstrip().split(','))) for line in sys.stdin]

    constellations = []
    for p in points:
        new_constellations = []
        constellation = [p]
        for c in constellations:
            if any(dist(p, p2) <= 3 for p2 in c):
                constellation.extend(c)
            else:
                new_constellations.append(c)
        new_constellations.append(constellation)
        constellations = new_constellations

    print(len(constellations))
