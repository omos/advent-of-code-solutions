#!/usr/bin/env python3

def get_result(recipes):
    scores = [3, 7]
    elves = [0, 1]

    while len(scores) < recipes + 10:
        total = sum(scores[pos] for pos in elves)
        scores.extend(map(int, str(total)))
        elves = [(pos + 1 + scores[pos]) % len(scores) for pos in elves]

    return ''.join(map(str, scores[recipes:recipes + 10]))

if __name__ == '__main__':
    assert get_result(9)    == '5158916779'
    assert get_result(2018) == '5941429882'

    print(get_result(704321))

