#!/usr/bin/env python3

def get_result(recipes):
    scores = [3, 7]
    elves = [0, 1]

    pattern = list(map(int, recipes))
    checked = len(scores)
    while True:
        total = sum(scores[pos] for pos in elves)
        scores.extend(map(int, str(total)))
        elves = [(pos + 1 + scores[pos]) % len(scores) for pos in elves]
        for i in range(checked, len(scores)):
            if scores[i - len(pattern):i] == pattern:
                return i - len(pattern)
        checked = max(checked, len(scores))

if __name__ == '__main__':
    assert get_result('51589') == 9
    assert get_result('01245') == 5
    assert get_result('92510') == 18
    assert get_result('59414') == 2018

    print(get_result('704321'))
