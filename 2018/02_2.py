#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    boxids = [line.strip() for line in sys.stdin]

    for i in range(len(boxids) - 1):
        for k in range(i + 1, len(boxids)):
            l = boxids[i]
            r = boxids[k]
            assert len(l) == len(r)
            res = ''.join(l[p] for p in range(len(l)) if l[p] == r[p])
            if len(l) - len(res) == 1:
                print(res)
                sys.exit(0)
