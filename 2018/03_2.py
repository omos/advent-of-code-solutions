#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    db = []
    overlap = set()
    for line in sys.stdin:
        l, r = line.strip().split(' @ ')
        cid0 = int(l.lstrip('#'))
        l, r = r.split(': ')
        x0, y0 = map(int, l.split(','))
        w0, h0 = map(int, r.split('x'))

        for cid1, x1, y1, w1, h1 in db:
            for x in range(max(x0, x1), min(x0 + w0, x1 + w1)):
                for y in range(max(y0, y1), min(y0 + h0, y1 + h1)):
                    overlap.add((x, y))
        db.append((cid0, x0, y0, w0, h0))

    for cid1, x1, y1, w1, h1 in db:
        if not any((x, y) in overlap for x in range(x1, x1 + w1) for y in range(y1, y1 + h1)):
            print(cid1)
            break
