#!/usr/bin/env python3

def get_highscore(players, last):
    scores = [0] * players
    circle = [0]
    pos = 0
    player = 0
    for m in range(1, last + 1):
        if m % 23 != 0:
            pos += 2
            pos %= len(circle)
            circle[pos:pos] = [m]
        else:
            scores[player] += m
            assert len(circle) >= 7
            pos += len(circle) - 7
            pos %= len(circle)
            scores[player] += circle[pos]
            del circle[pos]
        player += 1
        player %= players
    return max(scores)

if __name__ == '__main__':
    import sys

    assert get_highscore(9, 25) == 32
    assert get_highscore(10, 1618) == 8317
    assert get_highscore(13, 7999) == 146373
    assert get_highscore(17, 1104) == 2764
    assert get_highscore(21, 6111) == 54718
    assert get_highscore(30, 5807) == 37305
    print(get_highscore(476, 71657))
