#!/usr/bin/env python3

def dict_max_key(d):
    return max(d.items(), key=lambda i: i[1])[0]

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    current_guard = None
    fell_asleep = None
    total_asleep = defaultdict(int)
    minutes_asleep = defaultdict(lambda: defaultdict(int))
    for line in sorted(sys.stdin):
        timestamp, text = line.strip().split('] ')
        date, time = timestamp.lstrip('[').split(' ')
        year, month, day = map(int, date.split('-'))
        hour, minute = map(int, time.split(':'))
        words = text.split()

        if words[0] == 'Guard':
            assert fell_asleep == None
            current_guard = int(words[1].lstrip('#'))
        elif words[0] == 'falls':
            assert current_guard != None
            assert fell_asleep == None
            fell_asleep = minute
        elif words[0] == 'wakes':
            assert current_guard != None
            assert fell_asleep != None
            duration = minute - fell_asleep
            total_asleep[current_guard] += duration
            for m in range(fell_asleep, minute):
                minutes_asleep[current_guard][m] += 1
            fell_asleep = None

    chosen_guard = None
    chosen_minute = None
    maximum = 0
    for guard, minutes in minutes_asleep.items():
        m = dict_max_key(minutes_asleep[guard])
        value = minutes_asleep[guard][m]
        if value > maximum:
            maximum = value
            chosen_guard = guard
            chosen_minute = m

    print(chosen_guard * chosen_minute)
