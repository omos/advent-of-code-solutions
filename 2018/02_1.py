#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from collections import Counter

    count2 = 0
    count3 = 0
    for line in sys.stdin:
        boxid = line.strip()
        c = Counter(boxid)
        has2 = False
        has3 = False
        for k in c:
            if c[k] == 2:
                has2 = True
            elif c[k] == 3:
                has3 = True
        count2 += has2
        count3 += has3

    print(count2 * count3)
