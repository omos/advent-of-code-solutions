#!/usr/bin/env python3

TOOL_NEITHER = 0
TOOL_TORCH = 1
TOOL_GEAR = 2

ALLOWED = list(map(frozenset, ((TOOL_GEAR, TOOL_TORCH), (TOOL_NEITHER, TOOL_GEAR), (TOOL_NEITHER, TOOL_TORCH))))

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    depth = int(sys.stdin.readline().rstrip().split(' ')[1])
    tx, ty = tuple(map(int, sys.stdin.readline().rstrip().split(' ')[1].split(',')))

    w = tx + 1
    h = ty + 1

    cache = {}

    def get_erosion_level(x, y):
        if (x, y) in cache:
            return cache[(x, y)]

        if x == 0 and y == 0:
            gi = 0
        elif x == tx and y == ty:
            gi = 0
        elif y == 0:
            gi = x * 16807
        elif x == 0:
            gi = y * 48271
        else:
            gi = get_erosion_level(x - 1, y) * get_erosion_level(x, y - 1)
        el = (gi + depth) % 20183
        cache[(x, y)] = el
        return el

    paths = defaultdict(set)
    paths[0].add((0, 0, TOOL_TORCH))

    visited = set()
    next_time = 0
    while True:
        for p in paths[next_time]:
            if p in visited:
                continue
            visited.add(p)

            x, y, tool = p

            if tool == TOOL_TORCH and (x, y) == (tx, ty):
                print(next_time)
                sys.exit(0)

            rt = get_erosion_level(x, y) % 3
            for new_x, new_y in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
                if new_x < 0 or new_y < 0:
                    continue

                new_rt = get_erosion_level(new_x, new_y) % 3
                for new_tool in ALLOWED[rt] & ALLOWED[new_rt]:
                    new_time = next_time + (1 if new_tool == tool else 8)
                    paths[new_time].add((new_x, new_y, new_tool))
        paths[next_time].clear()
        next_time += 1
