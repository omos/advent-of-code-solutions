#!/usr/bin/env python3

def parse_node(cur):
    res = 0
    children = next(cur)
    meta = next(cur)
    for i in range(children):
        res += parse_node(cur)
    for i in range(meta):
        res += next(cur)
    return res

if __name__ == '__main__':
    import sys

    print(parse_node(iter(map(int, sys.stdin.read().strip().split(' ')))))
