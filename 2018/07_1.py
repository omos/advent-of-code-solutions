#!/usr/bin/env python3

def get_steps(deps):
    todo = list(deps)
    todo.sort()
    while len(todo):
        for i in range(len(todo)):
            if all(d not in todo for d in deps[todo[i]]):
                yield todo[i]
                del todo[i]
                break

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    deps = defaultdict(list)
    for req in sys.stdin:
        words = req.strip().split(' ')
        p = words[7]
        c = words[1]
        deps[p].append(c)
        deps[c].extend([])

    print(''.join(get_steps(deps)))
