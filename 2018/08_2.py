#!/usr/bin/env python3

def parse_node(cur):
    children = next(cur)
    meta = next(cur)

    values = [parse_node(cur) for i in range(children)]
    if children == 0:
        return sum(next(cur) for i in range(meta))

    res = 0
    for i in range(meta):
        i = next(cur)
        if i != 0 and i <= children:
            res += values[i - 1]
    return res

if __name__ == '__main__':
    import sys

    print(parse_node(iter(map(int, sys.stdin.read().strip().split(' ')))))
