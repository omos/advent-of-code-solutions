#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    depth = int(sys.stdin.readline().rstrip().split(' ')[1])
    tx, ty = tuple(map(int, sys.stdin.readline().rstrip().split(' ')[1].split(',')))

    w = tx + 1
    h = ty + 1

    cache = [[None] * w for i in range(h)]

    def get_erosion_level(x, y):
        if cache[y][x] != None:
            return cache[y][x]
        if x == 0 and y == 0:
            gi = 0
        elif x == tx and y == ty:
            gi = 0
        elif y == 0:
            gi = x * 16807
        elif x == 0:
            gi = y * 48271
        else:
            gi = get_erosion_level(x - 1, y) * get_erosion_level(x, y - 1)
        el = (gi + depth) % 20183
        cache[y][x] = el
        return el

    risk_level = 0
    for y in range(h):
        for x in range(w):
            risk_level += get_erosion_level(x, y) % 3
    print(risk_level)
