#!/usr/bin/env python3

def compute_level(s, x, y):
    rid = x + 10
    l = rid * (rid * y + s)
    l //= 100
    l %= 10
    return l - 5

def generate_powers(serial):
    power_levels = [[compute_level(serial, x, y) for y in range(300)] for x in range(300)]

    for size in range(1, 300):
        print(size)
        for x in range(300 - size + 1):
            power = sum(power_levels[x + dx][dy] for dx in range(size) for dy in range(size))
            yield power, x, 0, size
            for y in range(1, 300 - size + 1):
                power -= sum(power_levels[x + dx][y - 1] for dx in range(size))
                power += sum(power_levels[x + dx][y + size - 1] for dx in range(size))
                yield power, x, y, size

if __name__ == '__main__':
    SERIAL = 6042

    best_pos = None
    best_power = 0
    for power, x, y, size in generate_powers(SERIAL):
        if best_power == None or power > best_power:
            best_pos = (x, y, size)
            best_power = power

    print(','.join(map(str, best_pos)))
