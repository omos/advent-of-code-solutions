#!/usr/bin/env python3

def op_add(a, b):
    return a + b

def op_mul(a, b):
    return a * b

def op_ban(a, b):
    return a & b

def op_bor(a, b):
    return a | b

def op_set(a, b):
    return a

def op_gt(a, b):
    return 1 if a > b else 0

def op_eq(a, b):
    return 1 if a == b else 0

OPS = {
        'addi': (op_add, 0, 1),
        'addr': (op_add, 0, 0),
        'muli': (op_mul, 0, 1),
        'mulr': (op_mul, 0, 0),
        'bani': (op_ban, 0, 1),
        'banr': (op_ban, 0, 0),
        'bori': (op_bor, 0, 1),
        'borr': (op_bor, 0, 0),
        'seti': (op_set, 1, 1),
        'setr': (op_set, 0, 1),
        'gtrr': (op_gt, 0, 0),
        'gtri': (op_gt, 0, 1),
        'gtir': (op_gt, 1, 0),
        'eqrr': (op_eq, 0, 0),
        'eqri': (op_eq, 0, 1),
        'eqir': (op_eq, 1, 0),
    }

def exec_ins(ins, regs):
    op, va, vb = OPS[ins[0]]
    a = ins[1] if va else regs[ins[1]]
    b = ins[2] if vb else regs[ins[2]]
    regs[ins[3]] = op(a, b)

if __name__ == '__main__':
    import sys

    ip = int(sys.stdin.readline().strip().split(' ')[1])

    program = [(args[0],) + tuple(map(int, args[1:])) for line in sys.stdin for args in [line.strip().split(' ')]]

    regs = [0] * 6
    while 0 <= regs[ip] < len(program):
        if regs[ip] == 28:
            break
        ins = program[regs[ip]]
        exec_ins(ins, regs)
        regs[ip] += 1

    print(regs[2])
