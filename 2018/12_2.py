#!/usr/bin/env python3

PAT_EPS = 2
GENERATIONS = 50000000000

if __name__ == '__main__':
    import sys

    initial = sys.stdin.readline().strip().split(': ')[1]
    sys.stdin.readline()

    state = frozenset(i for i in range(len(initial)) if initial[i] == '#')
    pats = set()
    for line in sys.stdin:
        pat, r = line.strip().split(' => ')
        if r == '#':
            pats.add(tuple(p == '#' for p in pat))

    for g in range(GENERATIONS):
        l = min(state)
        r = max(state)

        new_state = frozenset(i for i in range(l - PAT_EPS, r + PAT_EPS + 1) if tuple((i + k) in state for k in range(-PAT_EPS, PAT_EPS + 1)) in pats)
        if frozenset(i - min(new_state) for i in new_state) == frozenset(i - min(state) for i in state):
            state = frozenset(i + (min(new_state) - min(state)) * (GENERATIONS - g) for i in state)
            break
        state = new_state

    print(sum(state))
