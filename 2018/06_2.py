#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    coords = list(tuple(map(int, line.strip().split(', '))) for line in sys.stdin)

    x0 = min(c[0] for c in coords)
    y0 = min(c[1] for c in coords)
    x1 = max(c[0] for c in coords)
    y1 = max(c[1] for c in coords)

    size = 0
    for x in range(x0, x1 + 1):
        for y in range(y0, y1 + 1):
            size += 1
            total_dist = 0
            for i in range(len(coords)):
                xx = coords[i][0]
                yy = coords[i][1]
                total_dist += abs(xx - x) + abs(yy - y)
                if total_dist >= 10000:
                    size -= 1
                    break

    print(size)
