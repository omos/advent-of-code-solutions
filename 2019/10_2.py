#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from math import gcd, atan2, sqrt
    from collections import defaultdict

    grid = [line.strip() for line in sys.stdin]
    h = len(grid)
    w = len(grid[0])

    asteroids = [(y, x)
                 for y in range(h)
                 for x in range(w)
                 if grid[y][x] == '#']

    best_count = 0
    best = None
    for by, bx in asteroids:
        count = 0
        for y, x in asteroids:
            if y == by and x == bx:
                continue
            dx = x - bx
            dy = y - by
            n = gcd(abs(dx), abs(dy))
            sx = dx // n
            sy = dy // n
            if all(grid[by + sy * i][bx + sx * i] == '.' for i in range(1, n)):
                count += 1
        if count > best_count:
            best_count = count
            best = by, bx


    def asteroid_dist(pos):
        y, x = pos
        by, bx = best

        rx = x - bx
        ry = y - by

        return sqrt(rx * rx + ry * ry)

    def asteroid_angle(pos):
        y, x = pos
        by, bx = best

        rx = x - bx
        ry = y - by

        return atan2(rx, ry)

    d = defaultdict(list)
    for pos in sorted(asteroids, key=asteroid_dist):
        d[asteroid_angle(pos)].append(pos)

    angles = list(d.keys())
    angles.sort(reverse=True)

    def find_nth(n):
        i = 0
        res = None
        while n > 0:
            div, mod = divmod(i, len(angles))
            l = d[angles[mod]]
            if div < len(l):
                n -= 1
                res = l[div]
            i += 1
        return res

    y, x = find_nth(200)
    print(y + 100 * x)
