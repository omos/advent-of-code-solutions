#!/usr/bin/env python3

from intcode import run_program
from collections import deque

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    print(list(run_program(code, deque([5])))[-1])
