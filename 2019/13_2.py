#!/usr/bin/env python3

from intcode import run_program
from collections import deque, defaultdict

class Debug(object):
    def __init__(self):
        self.area = defaultdict(int)

    def update_map(self, x, y, tile):
        self.area[(x, y)] = tile

    def print_map(self):
        from time import sleep
        sx = min(x for x, y in self.area)
        sy = min(y for x, y in self.area)
        ex = max(x for x, y in self.area)
        ey = max(y for x, y in self.area)
        for y in range(sy, ey + 1):
            print(''.join(' #x=o'[self.area[(x, y)]] for x in range(sx, ex + 1)))
        sleep(0.01)

class NoDebug(object):
    def update_map(self, x, y, tile):
        pass

    def print_map(self):
        pass

def signum(x):
    return (x > 0) - (x < 0)

if __name__ == '__main__':
    import sys

    if len(sys.argv) > 1 and sys.argv[1] == '--debug':
        dbg = Debug()
    else:
        dbg = NoDebug()

    code = list(map(int, sys.stdin.read().strip().split(',')))
    code[0] = 2

    score = 0
    ball = None
    paddle = None

    in_queue = deque()
    output = run_program(code, in_queue)
    try:
        while True:
            if paddle != None and ball != None:
                in_queue.appendleft(signum(ball[0] - paddle[0]))
                ball = None
                dbg.print_map()
            x = next(output)
            y = next(output)
            t = next(output)
            if x == -1 and y == 0:
                score = t
            else:
                if t == 3:
                    paddle = x, y
                elif t == 4:
                    ball = x, y
                dbg.update_map(x, y, t)
    except StopIteration:
        pass
    print(score)
