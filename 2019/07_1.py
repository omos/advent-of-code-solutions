#!/usr/bin/env python3

from intcode import run_program
from collections import deque

def run_config(code, phases):
    signal = 0
    for phase in phases:
        signal = list(run_program(code, deque([phase, signal])))[0]
    return signal

NR_AMPS = 5

if __name__ == '__main__':
    import sys
    from itertools import permutations

    code = list(map(int, sys.stdin.read().strip().split(',')))

    print(max(run_config(code, p) for p in permutations(range(NR_AMPS))))
