#!/usr/bin/env python3

from intcode import run_program
from collections import deque

DIRS = [(0, -1), (0, 1), (-1, 0), (1, 0)]
BOT_DIR = dict(zip('^v<>', DIRS))

FUNC_NAMES = 'ABC'

def build_map(code):
    scaffold = set()
    x, y = 0, 0
    bot_pos = None
    bot_dir = None
    for c in map(chr, run_program(code, None)):
        if c == '\n':
            x = 0
            y += 1
        else:
            if c != '.':
                if c in BOT_DIR:
                    bot_pos = x, y
                    bot_dir = BOT_DIR[c]
                scaffold.add((x, y))
            x += 1
    return scaffold, bot_pos, bot_dir

def build_full_program(scaffold, bot_pos, bot_dir):
    l = 0
    x, y = bot_pos
    dx, dy = bot_dir
    while scaffold:
        next_pos = (x + dx, y + dy)
        if next_pos in scaffold:
            l += 1
        else:
            for turn, dx, dy in [('L', dy, -dx), ('R', -dy, dx)]:
                next_pos = (x + dx, y + dy)
                if next_pos in scaffold:
                    if l > 0:
                        yield str(l)
                    l = 1
                    yield turn
                    break
        if (x + dy, y + dx) not in scaffold:
            scaffold.remove((x, y))
        x, y = next_pos
    if l > 0:
        yield str(l)

def optimize_program(program):
    solution = None
    for sa in range(0, len(program), 2):
        for ea in range(sa + 2, len(program), 2):
            if len(','.join(program[sa:ea])) >= 20:
                break
            for sb in range(ea, len(program), 2):
                for eb in range(sb + 2, len(program), 2):
                    if len(','.join(program[sb:eb])) >= 20:
                        break
                    for sc in range(eb, len(program), 2):
                        for ec in range(sc + 2, len(program), 2):
                            if len(','.join(program[sc:ec])) >= 20:
                                break
                            tmp = list(program)
                            la = ea - sa
                            lb = eb - sb
                            lc = ec - sc
                            for off in reversed(range(0, len(tmp) - la + 1)):
                                if tmp[off:off+la] == program[sa:ea]:
                                    tmp[off:off+la] = ['A']
                            for off in reversed(range(0, len(tmp) - lb + 1)):
                                if tmp[off:off+lb] == program[sb:eb]:
                                    tmp[off:off+lb] = ['B']
                            for off in reversed(range(0, len(tmp) - lc + 1)):
                                if tmp[off:off+lc] == program[sc:ec]:
                                    tmp[off:off+lc] = ['C']
                            if len(','.join(tmp)) < 20:
                                return ''.join(','.join(program) + '\n'
                                               for program in [tmp, program[sa:ea], program[sb:eb], program[sc:ec]]) + 'n\n'

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    scaffold, bot_pos, bot_dir = build_map(code)

    program = list(build_full_program(scaffold, bot_pos, bot_dir))

    inp = optimize_program(program)

    code[0] = 2
    print(list(run_program(code, deque(map(ord, inp))))[-1])
