#!/usr/bin/env python3

from intcode import run_program
from collections import deque

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    output = list(run_program(code, deque([2])))
    if len(output) != 1:
        print(output)
        assert False
    print(output[0])
