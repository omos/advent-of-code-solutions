#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    data = dict(line.strip().split(')')[::-1] for line in sys.stdin)

    total = 0
    for k in data:
        while k in data:
            k = data[k]
            total += 1
    print(total)
