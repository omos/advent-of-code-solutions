#!/usr/bin/env python3

DIR_TO_VEC = { 'U': (0, 1), 'D': (0, -1), 'L': (1, 0), 'R': (-1, 0) }

def walk_path(path):
    x, y = 0, 0
    steps = 0
    for step in path:
        dx, dy = DIR_TO_VEC[step[0]]
        length = int(step[1:])
        for i in range(length):
            x += dx
            y += dy
            steps += 1
            yield ((x, y), steps)

if __name__ == '__main__':
    import sys

    paths = [line.strip().split(',') for line in sys.stdin]

    dicts = [dict(walk_path(path)) for path in paths]
    intersections = dicts[0].keys() & dicts[1].keys()
    print(min(sum(d[p] for d in dicts) for p in intersections))
