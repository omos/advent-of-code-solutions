#!/usr/bin/env python3

from intcode import IntcodeRun
from collections import deque, defaultdict

NICS = 50

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    nics = [IntcodeRun(code) for i in range(NICS)]
    queues = defaultdict(deque, ((i, deque([i])) for i in range(NICS)))
    last_nats = deque(maxlen=2)
    nat = None

    while len(last_nats) < 2 or last_nats[0] != last_nats[1]:
        for i in range(NICS):
            if not queues[i]:
                queues[i].append(-1)
            nic = nics[i].process_input(queues[i])
            try:
                while True:
                    addr = next(nic)
                    packet = next(nic), next(nic)
                    if addr == 255:
                        nat = packet
                    else:
                        queues[addr].extend(packet)
            except StopIteration:
                pass
        if not any(queues.values()) and nat != None:
            last_nats.append(nat)
            queues[0].extend(nat)
            nat = None
    print(last_nats[0][1])
