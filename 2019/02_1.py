#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    code[1] = 12
    code[2] = 2

    pos = 0
    while code[pos] != 99:
        p1 = code[pos + 1]
        p2 = code[pos + 2]
        pr = code[pos + 3]
        if code[pos] == 1:
            code[pr] = code[p1] + code[p2]
        else:
            code[pr] = code[p1] * code[p2]
        pos += 4
    print(code[0])
