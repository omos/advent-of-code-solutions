#!/usr/bin/env python3

IMG_W = 25
IMG_H = 6

IMG_SIZE = IMG_W * IMG_H

COLOR_MAP = { '0': ' ', '1': '#' }

if __name__ == '__main__':
    from collections import Counter
    import sys

    data = sys.stdin.read().strip()
    layers = [data[start:start+IMG_SIZE] for start in range(0, len(data), IMG_SIZE)]

    res = [None] * IMG_SIZE
    for layer in layers:
        for i in range(IMG_SIZE):
            if res[i] == None and layer[i] != '2':
                res[i] = COLOR_MAP[layer[i]]

    print('\n'.join(''.join(res[start:start+IMG_W]) for start in range(0, IMG_SIZE, IMG_W)))
