#!/usr/bin/env python3

def divceil(a, b):
    return a // b + bool(a % b)

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    reactions = dict((out_el, (int(out_n), [(int(inp_n), inp_el)
                                            for i in inp.split(', ')
                                            for inp_n, inp_el in [i.split(' ')]]))
                     for line in sys.stdin
                     for inp, out in [line.strip().split(' => ')]
                     for out_n, out_el in [out.split(' ')])

    deps = { 'ORE': frozenset() }
    for out_el in reactions.keys():
        stack = set([out_el])
        res = set(['ORE'])
        while len(stack):
            el = stack.pop()
            out_n, inputs = reactions[el]
            new = frozenset(inp_el for inp_n, inp_el in inputs)
            stack.update(new - res)
            res.update(new)
        deps[out_el] = res

    ore = 0
    have = defaultdict(int)
    fuel = 0
    next_needed = 1
    while next_needed:
        need = defaultdict(int, { 'FUEL': next_needed })
        while sum(n for el, n in need.items() if el != 'ORE') != 0:
            all_deps = set(d for el, n in need.items() if n > 0 for d in deps[el])
            for el, n in list(need.items()):
                if el in all_deps:
                    continue
                if have[el] >= n:
                    have[el] -= n
                    continue

                n -= have[el]
                need[el] = 0

                out_n, inputs = reactions[el]
                react_n = divceil(n, out_n)
                have[el] = react_n * out_n - n
                for inp_n, inp_el in inputs:
                    need[inp_el] += react_n * inp_n
        ore += need['ORE']
        fuel += next_needed
        next_needed = int(10 ** 12 * fuel / ore) - fuel
    print(fuel)
