#!/usr/bin/env python3

DIRS = [(-1, 0), (1, 0), (0, -1), (0, 1)]

def is_outer(r, c, w, h):
    return 2 in (r, h - r - 1, c, w - c - 1)

def generate_neighbors(cursors, corridors, portals_in, portals_out):
    for r, c, level in cursors:
        for dr, dc in DIRS:
            pos = (r + dr, c + dc, level)
            if pos[:2] in corridors:
                yield pos
        label = corridors[(r, c)]
        if label != None:
            if not is_outer(r, c, w, h):
                yield portals_out[label]  + (level + 1,)
            elif level > 0:
                yield portals_in[label] + (level - 1,)

if __name__ == '__main__':
    import sys

    grid = [line.strip('\n') for line in sys.stdin]
    h = len(grid)
    w = max(len(line) for line in grid)

    corridors = {}
    portals_out = {}
    portals_in = {}
    for r in range(2, h - 2):
        for c in range(2, w - 2):
            if grid[r][c] != '.':
                continue
            corridors[(r, c)] = None
            for dr, dc in DIRS:
                label = ''.join(grid[r + i * dr][c + i * dc] for i in range(1, 3))
                if label == 'AA':
                    start = r, c, 0
                elif label == 'ZZ':
                    end = r, c, 0
                elif all(c.isupper() for c in label):
                    label = label[::(dr + dc)]
                    corridors[(r, c)] = label
                    if is_outer(r, c, w, h):
                        portals_out[label] = (r, c)
                    else:
                        portals_in[label] = (r, c)

    dist = 0
    visited = set()
    cursors = set([start])
    while end not in cursors:
        visited.update(cursors)
        cursors = set(pos
                      for pos in generate_neighbors(cursors, corridors,
                                                    portals_in, portals_out)
                      if pos not in visited)
        dist += 1

    print(dist)
