#!/usr/bin/env python3

def signum(x):
    return (x > 0) - (x < 0)

DIM = 3

if __name__ == '__main__':
    import sys
    from itertools import combinations
    from math import gcd

    pos = [[int(c.split('=')[-1]) for c in line.strip('<>\n').split(', ')]
           for line in sys.stdin]
    count = len(pos)

    # transpose to separate by dimension:
    pos = [[pos[i][d] for i in range(count)] for d in range(DIM)]
    vel = [[0] * count for d in range(DIM)]

    cycles = []
    for d in range(DIM):
        p = pos[d]
        v = vel[d]

        initial_state = (tuple(p), tuple(v))

        step = 0
        while True:
            for i, k in combinations(range(count), 2):
                di = signum(p[k] - p[i])
                v[i] += di
                v[k] -= di
            for i in range(count):
                p[i] += v[i]
            step += 1
            if (tuple(p), tuple(v)) == initial_state:
                break
        cycles.append(step)

    def lcm(a, b):
        return a * b // gcd(a, b)

    print(lcm(lcm(cycles[0], cycles[1]), cycles[2]))
