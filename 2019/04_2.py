#!/usr/bin/env python3

INPUT_MIN = 240920
INPUT_MAX = 789857

def password_matches(p):
    s = str(p)

    if not any(s.find(d * 2) >= 0 and s.find(d * 3) < 0 for d in '123456789'):
        return False

    return all(s[i] <= s[i + 1] for i in range(5))

if __name__ == '__main__':
    print(sum(password_matches(i) for i in range(INPUT_MIN, INPUT_MAX + 1)))
