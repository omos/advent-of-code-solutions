#!/usr/bin/env python3

from intcode import run_program
from collections import deque

def generate_l1_exps(l0_exps, l1_exps):
    for k1, v1 in l0_exps.items():
        for k2, v2 in l1_exps.items():
            yield k1 & k2, ('AND1', v1, v2)
            yield k1 | k2, ('OR1',  v1, v2)
    for k1, v1 in l1_exps.items():
        yield ~k1 & 0xFFFF, ('NOT1', v1)

def generate_init_l2_exps(l1_exps):
    for k1, v1 in l1_exps.items():
        for k2, v2 in l1_exps.items():
            yield k1 & k2, ('AND2', v1, v2)
            yield k1 | k2, ('OR2',  v1, v2)

def generate_l2_exps(l0_exps, l1_exps, l2_exps):
    for k1, v1 in l1_exps.items():
        for k2, v2 in l2_exps.items():
            yield k1 & k2, ('AND2', v1, v2)
            yield k1 | k2, ('OR2',  v1, v2)
    for k1, v1 in l0_exps.items():
        for k2, v2 in l2_exps.items():
            yield k1 & k2, ('AND1', v1, v2)
            yield k1 | k2, ('OR1',  v1, v2)

def generate_expressions():
    cache = set()
    l0_exps = {
        0b1010101010101010: 'A',
        0b1100110011001100: 'B',
        0b1111000011110000: 'C',
        0b1111111100000000: 'D',
    }
    l1_exps = {}
    l2_exps = {}
    for k1, v1 in l0_exps.items():
        k = ~k1 & 0xFFFF
        cache.add(k)
        l1_exps[k] = ('NOT0', v1)
    while True:
        new_exps = []
        for k, exp in generate_l1_exps(l0_exps, l1_exps):
            if k not in cache:
                yield exp
                cache.add(k)
                new_exps.append((k, exp))
        if len(new_exps) == 0:
            break
        l1_exps.update(new_exps)
    for k, exp in generate_init_l2_exps(l1_exps):
        if k not in cache:
            yield exp
            cache.add(k)
            l2_exps[k] = exp
    while True:
        new_exps = []
        for k, exp in generate_l2_exps(l0_exps, l1_exps, l2_exps):
            if k not in cache:
                yield exp
                cache.add(k)
                new_exps.append((k, exp))
        if len(new_exps) == 0:
            break
        l2_exps.update(new_exps)
        print(len(cache))
    for k1, v1 in l2_exps.items():
        k = ~k1 & 0xFFFF
        exp = ('NOT2', v1)
        if k not in cache:
            yield exp
            cache.add(k)

def compile_expression(exp, dst='J'):
    if exp[0] == 'NOT0':
        yield 'NOT {} {}'.format(exp[1], dst)
    elif exp[0] == 'AND1':
        for l in compile_expression(exp[2], 'T'):
            yield l
        yield 'AND {} T'.format(exp[1])
        if dst != 'T':
            yield 'OR T {}'.format(dst)
    elif exp[0] == 'OR1':
        for l in compile_expression(exp[2], 'T'):
            yield l
        yield 'OR {} T'.format(exp[1])
        if dst != 'T':
            yield 'OR T {}'.format(dst)
    elif exp[0] == 'NOT1':
        for l in compile_expression(exp[1], 'T'):
            yield l
        yield 'NOT T {}'.format(dst)
    elif exp[0] == 'AND2':
        for l in compile_expression(exp[2]):
            yield l
        for l in compile_expression(exp[1], 'T'):
            yield l
        yield 'AND T J'
    elif exp[0] == 'OR2':
        for l in compile_expression(exp[2]):
            yield l
        for l in compile_expression(exp[1], 'T'):
            yield l
        yield 'OR T J'
    elif exp[0] == 'NOT2':
        for l in compile_expression(exp[1]):
            yield l
        yield 'NOT J J'
    else:
        assert False

def check_script(code, script):
    output = list(run_program(code, deque(map(ord, script))))
    return output[-1] if output[-1] >= 0x80 else None

def solve(code):
    for exp in generate_expressions():
        script = '\n'.join(compile_expression(exp)) + '\nWALK\n'
        res = check_script(code, script)
        if res != None:
            print(script)
            return res
    return None

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    print(solve(code))
