#!/usr/bin/env python3

from intcode import run_program
from collections import deque

def run_config(code, phases):
    inputs = [deque([p]) for p in phases]
    runs = [run_program(code, inp) for inp in inputs]
    idx = 0
    last_signal = 0
    while True:
        inputs[idx].append(last_signal)
        try:
            last_signal = next(runs[idx])
        except StopIteration:
            assert idx == 0
            return last_signal
        idx += 1
        idx %= len(runs)

if __name__ == '__main__':
    import sys
    from itertools import permutations

    code = list(map(int, sys.stdin.read().strip().split(',')))

    print(max(run_config(code, p) for p in permutations(range(5, 10))))
