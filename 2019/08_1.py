#!/usr/bin/env python3

IMG_W = 25
IMG_H = 6

IMG_SIZE = IMG_W * IMG_H

if __name__ == '__main__':
    from collections import Counter
    import sys

    data = sys.stdin.read().strip()
    layers = [data[start:start+IMG_SIZE] for start in range(0, len(data), IMG_SIZE)]

    tgt = min(map(Counter, layers), key=lambda c: c['0'])
    print(tgt['1'] * tgt['2'])
