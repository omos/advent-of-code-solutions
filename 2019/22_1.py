#!/usr/bin/env python3

if __name__ == '__main__':
    from collections import deque
    import sys

    cards = deque(range(10007))

    for line in sys.stdin:
        line = line.rstrip('\n').split(' ')
        if line[0] == 'cut':
            n = int(line[1])
            cards.rotate(-n)
        elif line[1] == 'into':
            cards.reverse()
        else:
            n = int(line[3])
            new_cards = [None] * len(cards)
            for i, c in enumerate(cards):
                new_cards[(i * n) % len(cards)] = c
            assert all(c != None for c in new_cards)
            cards = deque(new_cards)
    print(cards.index(2019))
