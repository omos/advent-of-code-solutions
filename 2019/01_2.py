#!/usr/bin/env python3

def get_fuel(mass):
    fuel = 0
    while True:
        mass = mass // 3 - 2
        if mass <= 0:
            break
        fuel += mass
    return fuel

if __name__ == '__main__':
    import sys

    print(sum(get_fuel(int(line.strip())) for line in sys.stdin))
