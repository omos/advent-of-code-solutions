#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    signal = sys.stdin.read().strip() * 10000
    offset = int(signal[:7])
    signal = list(map(int, signal[offset:]))
    for phase in range(100):
        for k in reversed(range(len(signal) - 1)):
            signal[k] = (signal[k] + signal[k + 1]) % 10
    print(''.join(map(str, signal[:8])))
