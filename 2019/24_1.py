#!/usr/bin/env python3

def to_biodiv(layout):
    return sum(1 << i for i in range(25) if layout[i // 5][i % 5])

def get_adjacent(layout, r, c):
    for dr, dc in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
        if 0 <= r + dr < 5 and 0 <= c + dc < 5:
            yield layout[r + dr][c + dc]
        else:
            yield False

if __name__ == '__main__':
    import sys

    layout = [[c == '#' for c in line.strip()] for line in sys.stdin]

    history = set()
    while True:
        bd = to_biodiv(layout)
        if bd in history:
            break
        history.add(bd)

        layout =[[sum(get_adjacent(layout, r, c)) in ((1,) if layout[r][c] else (1, 2))
                  for c in range(5)] for r in range(5)]
    print(bd)


    
