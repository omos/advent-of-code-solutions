#!/usr/bin/env python3

NCARDS = 119315717514047 # note that this is a prime number
NREPEATS = 101741582076661

if __name__ == '__main__':
    import sys

    # index_after = k * index_before + q (mod NCARDS)
    k, q = 1, 0
    for line in sys.stdin:
        line = line.rstrip('\n').split(' ')
        if line[0] == 'cut':
            n = int(line[1])
            q = (q - n) % NCARDS
        elif line[1] == 'into':
            k = -k % NCARDS
            q = (-q - 1) % NCARDS
        else:
            n = int(line[3])
            k = (k * n) % NCARDS
            q = (q * n) % NCARDS

    # k * x + q applied NREPEATS times == kr * x + qr
    # WHERE:
    # kr = k^n (mod NCARDS)
    # qr = q * sum_0^(n-1) k^n = q * (k^n - 1)/(k - 1) (mod NCARDS)
    kr = pow(k, NREPEATS, NCARDS)
    qr = (q * (kr - 1) * pow(k - 1, NCARDS - 2, NCARDS)) % NCARDS

    # answer = (2020 - q) / k (mod NCARDS)
    print(((2020 - qr) * pow(kr, NCARDS - 2, NCARDS)) % NCARDS)
