#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from math import gcd

    grid = [line.strip() for line in sys.stdin]
    h = len(grid)
    w = len(grid[0])

    best_count = 0
    for by in range(h):
        for bx in range(w):
            if grid[by][bx] == '.':
                continue

            count = 0
            for y in range(h):
                for x in range(w):
                    if grid[y][x] == '.' or (y == by and x == bx):
                        continue
                    dx = x - bx
                    dy = y - by
                    n = gcd(abs(dx), abs(dy))
                    sx = dx // n
                    sy = dy // n
                    if all(grid[by + sy * i][bx + sx * i] == '.'
                           for i in range(1, n)):
                        count += 1
            if count > best_count:
                best_count = count
    print(best_count)
    
