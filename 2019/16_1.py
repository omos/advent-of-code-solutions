#!/usr/bin/env python3

from itertools import *

BASE_PATTERN = [0, 1, 0, -1]

def pattern(k):
    return islice(chain.from_iterable(repeat(i, k + 1) for i in cycle(BASE_PATTERN)), 1, None)

def fft(signal):
    for k in range(len(signal)):
        yield abs(sum(a * b for a, b in zip(signal, pattern(k)))) % 10

def fft_str(signal, n=1):
    signal = list(map(int, signal))
    for i in range(n):
        signal = list(fft(signal))
    return ''.join(map(str, signal))

if __name__ == '__main__':
    import sys

    assert fft_str('12345678') == '48226158'
    assert fft_str('80871224585914546619083218645595', 100)[:8] == '24176176'
    assert fft_str('19617804207202209144916044189917', 100)[:8] == '73745418'
    assert fft_str('69317163492948606335995924319873', 100)[:8] == '52432133'

    print(fft_str(sys.stdin.read().strip(), 100)[:8])
