from collections import defaultdict

class IntcodeRun(object):
    def __init__(self, code):
        self.code = defaultdict(int, ((i, code[i]) for i in range(len(code))))
        self.pos = 0
        self.base = 0

    def finished(self):
        return self.code[self.pos] == 99

    def _fetch(self, val, mode):
        if mode == 0:
            return self.code[val]
        if mode == 1:
            return val
        if mode == 2:
            return self.code[val + self.base]
        assert False

    def _store(self, pos, val, mode):
        if mode == 0:
            self.code[pos] = val
        elif mode == 2:
            self.code[pos + self.base] = val
        else:
            assert False

    def process_input(self, in_queue):
        while not self.finished():
            insn = self.code[self.pos]
            opcode = insn % 100; insn //= 100
            mode_0 = insn %  10; insn //=  10
            mode_1 = insn %  10; insn //=  10
            mode_2 = insn %  10; insn //=  10
            if opcode in (1, 2):
                p0 = self._fetch(self.code[self.pos + 1], mode_0)
                p1 = self._fetch(self.code[self.pos + 2], mode_1)
                pr = self.code[self.pos + 3]

                self._store(pr, p0 + p1 if opcode == 1 else p0 * p1, mode_2)
                self.pos += 4
            elif opcode == 3:
                pr = self.code[self.pos + 1]

                if not in_queue:
                    break

                self._store(pr, in_queue.popleft(), mode_0)
                self.pos += 2
            elif opcode == 4:
                yield self._fetch(self.code[self.pos + 1], mode_0)

                self.pos += 2
            elif opcode in (5, 6):
                p0 = self._fetch(self.code[self.pos + 1], mode_0)
                p1 = self._fetch(self.code[self.pos + 2], mode_1)

                if p0 if opcode == 5 else not p0:
                    self.pos = p1
                else:
                    self.pos += 3
            elif opcode in (7, 8):
                p0 = self._fetch(self.code[self.pos + 1], mode_0)
                p1 = self._fetch(self.code[self.pos + 2], mode_1)
                pr = self.code[self.pos + 3]

                self._store(pr, int(p0 < p1 if opcode == 7 else p0 == p1), mode_2)
                self.pos += 4
            elif opcode == 9:
                p0 = self._fetch(self.code[self.pos + 1], mode_0)

                self.base += p0
                self.pos += 2
            else:
                assert False

def run_program(code, in_queue):
    run = IntcodeRun(code)
    for out in run.process_input(in_queue):
        yield out
    assert run.finished()

__all__ = ('IntcodeRun', 'run_program')
