#!/usr/bin/env python3

EXPECTED_OUTPUT = 19690720

def run_program(code, i1, i2):
    code = list(code)
    code[1] = i1
    code[2] = i2

    pos = 0
    while code[pos] != 99:
        p1 = code[pos + 1]
        p2 = code[pos + 2]
        pr = code[pos + 3]
        if code[pos] == 1:
            code[pr] = code[p1] + code[p2]
        else:
            code[pr] = code[p1] * code[p2]
        pos += 4
    return code[0]

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    res = None
    for noun in range(100):
        for verb in range(100):
            if run_program(code, noun, verb) == EXPECTED_OUTPUT:
                res = noun * 100 + verb
                break

    assert res != None
    print(res)
