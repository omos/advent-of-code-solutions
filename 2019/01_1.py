#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    print(sum(int(line.strip()) // 3 - 2 for line in sys.stdin))
