#!/usr/bin/env python3

DIRS = [(-1, 0), (1, 0), (0, -1), (0, 1)]

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    grid = [line.strip('\n') for line in sys.stdin]
    h = len(grid)
    w = max(len(line) for line in grid)

    corridors = {}
    portals = defaultdict(list)
    for r in range(2, h - 2):
        for c in range(2, w - 2):
            if grid[r][c] != '.':
                continue
            corridors[(r, c)] = None
            for dr, dc in DIRS:
                label = ''.join(grid[r + i * dr][c + i * dc] for i in range(1, 3))
                if label == 'AA':
                    start = r, c
                elif label == 'ZZ':
                    end = r, c
                elif all(c.isupper() for c in label):
                    label = label[::(dr + dc)]
                    corridors[(r, c)] = label
                    portals[label].append((r, c))
    assert all(len(v) == 2 for v in portals.values())

    dist = 0
    visited = set()
    cursors = set([start])
    while end not in cursors:
        visited.update(cursors)
        new_cursors = set()
        for r, c in cursors:
            for dr, dc in DIRS:
                pos = (r + dr, c + dc)
                if pos in corridors and pos not in visited:
                    new_cursors.add(pos)
            label = corridors[(r, c)]
            if label != None:
                ends = portals[label]
                pos = ends[~ends.index((r, c))]
                if pos not in visited:
                    new_cursors.add(pos)
        cursors = new_cursors
        dist += 1

    print(dist)
