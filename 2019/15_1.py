#!/usr/bin/env python3

from intcode import run_program
from collections import defaultdict, deque

DIRS = [None, (0, -1), (0, 1), (-1, 0), (1, 0)]

MAP_UNKNOWN = 0
MAP_MARKED  = 1
MAP_FREE    = 2
MAP_WALL    = 3
MAP_START   = 4
MAP_TARGET  = 5

VISITED_WALL = 1 << 31

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    area = defaultdict(int)
    area[(0, 0)] = MAP_START
    visited = defaultdict(int)
    visited[(0, 0)] = 1
    in_queue = deque()

    x, y = 0, 0
    to_check = 1
    direction = 1
    dx, dy = DIRS[direction]
    area[(dx, dy)] = MAP_MARKED
    target_pos = None

    output = run_program(code, in_queue)
    while to_check > 0:
        in_queue.appendleft(direction)
        status = next(output)
        x += dx
        y += dy
        if status == 0:
            if area[(x, y)] == MAP_MARKED:
                to_check -= 1
            visited[(x, y)] = VISITED_WALL
            area[(x, y)] = MAP_WALL
            x -= dx
            y -= dy
        else:
            visited[(x, y)] += 1
            assert area[(x, y)] != MAP_UNKNOWN
            if area[(x, y)] == MAP_MARKED:
                to_check -= 1
                if status == 2:
                    area[(x, y)] = MAP_TARGET
                    target_pos = (x, y)
                else:
                    area[(x, y)] = MAP_FREE
        min_visited = VISITED_WALL
        min_dir = None
        for d in range(1, 5):
            dx, dy = DIRS[d]
            v = visited[(x + dx, y + dy)]
            if area[(x + dx, y + dy)] == MAP_UNKNOWN:
                to_check += 1
                area[(x + dx, y + dy)] = MAP_MARKED
            if v < min_visited:
                min_dir = d, dx, dy
                min_visited = v
        direction, dx, dy = min_dir

    todo = [(0, 0)]
    visited = set()
    dist = 0
    while not target_pos in todo:
        new_todo = []
        for x, y in todo:
            visited.add((x, y))
            for dx, dy in DIRS[1:5]:
                neighbor = (x + dx, y + dy)
                if area[neighbor] != MAP_WALL and neighbor not in visited:
                    new_todo.append(neighbor)
        todo = new_todo
        dist += 1
    print(dist)
