#!/usr/bin/env python3

def find_reachable(grid, start, keyring):
    dist = 0
    visited = set()
    cursors = [start]
    while cursors:
        visited.update(cursors)
        dist += 1
        new_cursors = set()
        for r, c in cursors:
            for dr, dc in [(1, 0), (-1, 0), (0, 1), (0, -1)]:
                pos = (r + dr, c + dc)
                if pos in visited:
                    continue
                v = grid[pos[0]][pos[1]]
                if v == '#':
                    continue
                if v.isupper() and v.lower() not in keyring:
                    continue
                if v.islower() and v not in keyring:
                    yield (v, dist, pos)
                    continue
                new_cursors.add(pos)
        cursors = new_cursors

def find_shortest_path(grid, start):
    key_count = len(frozenset(c for line in grid for c in line if c.islower()))
    states = { (frozenset(), start): 0 }
    for i in range(key_count):
        new_states = {}
        for (keyring, pos), dist in states.items():
            for key, d, pos in find_reachable(grid, pos, keyring):
                st_key = (keyring | frozenset([key]), pos)
                if st_key not in new_states or new_states[st_key] > dist + d:
                    new_states[st_key] = dist + d
        states = new_states
    return min(states.values())

if __name__ == '__main__':
    import sys

    grid = sys.stdin.read().strip().split('\n')
    for r in range(len(grid)):
        for c in range(len(grid[r])):
            if grid[r][c] == '@':
                start = (r, c)

    print(find_shortest_path(grid, start))
