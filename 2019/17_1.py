#!/usr/bin/env python3

from intcode import run_program

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    scaffold = set()
    x, y = 0, 0

    for c in map(chr, run_program(code, None)):
        if c == '\n':
            x = 0
            y += 1
        else:
            if c != '.':
                scaffold.add((x, y))
            x += 1
    print(sum(x * y
              for x, y in scaffold
              if all(p in scaffold
                     for p in [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)])))
