#!/usr/bin/env python3

from intcode import run_program
from collections import deque, defaultdict

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    area = defaultdict(int)
    output = run_program(code, None)
    try:
        while True:
            x = next(output)
            y = next(output)
            t = next(output)
            area[(x, y)] = t
    except StopIteration:
        pass

    print(sum(v == 2 for v in area.values()))
