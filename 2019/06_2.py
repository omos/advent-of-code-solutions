#!/usr/bin/env python3

def path_to_com(data, k):
    while k in data:
        k = data[k]
        yield k

if __name__ == '__main__':
    import sys

    data = dict(line.strip().split(')')[::-1] for line in sys.stdin)

    path_to_san = list(path_to_com(data, 'SAN'))[::-1]
    path_to_you = list(path_to_com(data, 'YOU'))[::-1]
    start = 0
    while path_to_san[start] == path_to_you[start]:
        start += 1
    print(len(path_to_san) + len(path_to_you) - 2 * start)
