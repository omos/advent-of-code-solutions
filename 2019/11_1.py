#!/usr/bin/env python3

from intcode import run_program
from collections import deque, defaultdict

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    area = defaultdict(int)
    in_queue = deque()

    x, y = 0, 0
    direction = 0

    DIRS = [(0, -1), (1, 0), (0, 1), (-1, 0)]

    output = run_program(code, in_queue)
    try:
        while True:
            in_queue.appendleft(area[(x, y)])
            color = next(output)
            turn = next(output)
            area[(x, y)] = color
            direction += 1 if turn else -1
            dx, dy = DIRS[direction % len(DIRS)]
            x += dx
            y += dy
    except StopIteration:
        pass
    print(len(area))
