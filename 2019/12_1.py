#!/usr/bin/env python3

def signum(x):
    return (x > 0) - (x < 0)

DIM = 3

if __name__ == '__main__':
    import sys
    from itertools import combinations

    pos = [[int(c.split('=')[-1]) for c in line.strip('<>\n').split(', ')]
           for line in sys.stdin]
    count = len(pos)
    vel = [[0] * DIM for i in range(count)]

    for step in range(1000):
        for i, k in combinations(range(count), 2):
            for d in range(DIM):
                di = signum(pos[k][d] - pos[i][d])
                vel[i][d] += di
                vel[k][d] -= di
        for i in range(count):
            for d in range(DIM):
                pos[i][d] += vel[i][d]

    print(sum(sum(map(abs, pos[i])) * sum(map(abs, vel[i]))
              for i in range(count)))
