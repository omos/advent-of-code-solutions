#!/usr/bin/env python3

from intcode import run_program
from collections import deque

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    print(sum(sum(run_program(code, deque((x, y))))
              for x in range(50)
              for y in range(50)))
