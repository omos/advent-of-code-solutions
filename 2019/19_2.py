#!/usr/bin/env python3

from intcode import run_program
from collections import deque

SHIP_SIZE = 100

def eval_pos(code, x, y):
    return sum(run_program(code, deque((x, y))))

def find_start(code):
    for x in range(1, 50):
        for y in range(1, 50):
            if eval_pos(code, x, y):
                return x, y

def solve(code, x, y):
    history = deque(maxlen=SHIP_SIZE)
    x_start = x
    x_end = x + 1
    while len(history) < SHIP_SIZE or history[0] - x_start < SHIP_SIZE:
        while not eval_pos(code, x_start, y):
            x_start += 1

        while eval_pos(code, x_end, y):
            x_end += 1

        history.append(x_end)
        y += 1
    return x_start, y - SHIP_SIZE

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    x, y = solve(code, *find_start(code))
    print(x * 10000 + y)
