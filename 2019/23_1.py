#!/usr/bin/env python3

from intcode import IntcodeRun
from collections import deque, defaultdict

NICS = 50

if __name__ == '__main__':
    import sys

    code = list(map(int, sys.stdin.read().strip().split(',')))

    nics = [IntcodeRun(code) for i in range(NICS)]
    queues = defaultdict(deque, ((i, deque([i])) for i in range(NICS)))

    while not queues[255]:
        for i in range(NICS):
            if not queues[i]:
                queues[i].append(-1)
            nic = nics[i].process_input(queues[i])
            try:
                while True:
                    addr = next(nic)
                    x = next(nic)
                    y = next(nic)
                    queues[addr].extend((x, y))
            except StopIteration:
                pass
    print(queues[255][1])
