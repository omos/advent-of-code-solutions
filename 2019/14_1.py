#!/usr/bin/env python3

def divceil(a, b):
    return a // b + bool(a % b)

if __name__ == '__main__':
    import sys
    from collections import defaultdict

    reactions = dict((out_el, (int(out_n), [(int(inp_n), inp_el)
                                            for i in inp.split(', ')
                                            for inp_n, inp_el in [i.split(' ')]]))
                     for line in sys.stdin
                     for inp, out in [line.strip().split(' => ')]
                     for out_n, out_el in [out.split(' ')])

    deps = { 'ORE': frozenset() }
    for out_el in reactions.keys():
        stack = set([out_el])
        res = set(['ORE'])
        while len(stack):
            el = stack.pop()
            out_n, inputs = reactions[el]
            new = frozenset(inp_el for inp_n, inp_el in inputs)
            stack.update(new - res)
            res.update(new)
        deps[out_el] = res

    need = { 'FUEL': 1 }
    while len(need) != 1 or 'ORE' not in need:
        new_need = defaultdict(int)
        all_deps = set(d for el in need for d in deps[el])
        for el, n in need.items():
            if el in all_deps:
                new_need[el] += n
                continue
            out_n, inputs = reactions[el]
            react_n = divceil(n, out_n)
            for inp_n, inp_el in inputs:
                new_need[inp_el] += react_n * inp_n
        need = new_need
    print(need['ORE'])
