#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    print(sum(2 + line.count('\\') + line.count('"') for line in sys.stdin))