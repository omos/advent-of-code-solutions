#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    total = 0
    for line in sys.stdin:
        l, w, h = map(int, line.split('x'))
        a, b = sorted((l, w, h))[:2]
        total += 2*a + 2*b + l*w*h
    print total