#!/usr/bin/env python3

import re

def is_nice(s):
    if re.search(r'(..).*\1', s) == None:
        return False
    if re.search(r'(.).\1', s) == None:
        return False
    return True

if __name__ == '__main__':
    import sys
    
    print(sum(is_nice(line) for line in sys.stdin))
