#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    x = [0, 0]
    y = [0, 0]
    visited = [set([(0,0)]), set([(0,0)])]
    i = 0
    for l in sys.stdin:
        for c in l:
            if c == '>':
                x[i] += 1
            elif c == '<':
                x[i] -= 1
            elif c == '^':
                y[i] += 1
            elif c == 'v':
                y[i] -= 1
            visited[i].add((x[i], y[i]))
            i = ~i & 1
    
    print(len(visited[0] | visited[1]))