#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    grid = [[False] * 1000 for i in range(1000)]
    for line in sys.stdin:
        fields = line.split(' ')
        i = 0
        cmd = fields[i]
        i += 1
        if cmd == 'turn':
            cmd += ' ' + fields[i]
            i += 1
        
        x0, y0 = map(int, fields[i].split(','))
        i += 2
        x1, y1 = map(int, fields[i].split(','))
        
        if cmd == 'toggle':
            f = lambda x: not x
        elif cmd == 'turn on':
            f = lambda x: True
        elif cmd == 'turn off':
            f = lambda x: False
        else:
            assert False
        for x in range(x0, x1 + 1):
            for y in range(y0, y1 + 1):
                grid[x][y] = f(grid[x][y])
    print(sum(sum(row) for row in grid))
