#!/usr/bin/env python3

import re
import operator

class Expr(object):
    def __init__(self):
        self.cached_val = None
    
    def eval(self, wires):
        if self.cached_val != None:
            return self.cached_val
        self.cached_val = self.do_eval(wires)
        return self.cached_val

    def do_eval(self, wires):
        raise NotImplementedError()

class ConstExpr(Expr):
    def __init__(self, val):
        Expr.__init__(self)
        self.val = val
    
    def do_eval(self, wires):
        return self.val

class WireExpr(Expr):
    def __init__(self, label):
        Expr.__init__(self)
        self.label = label
    
    def do_eval(self, wires):
        return wires[self.label].eval(wires)

class FuncExpr(Expr):
    def __init__(self, f, *ops):
        Expr.__init__(self)
        self.f = f
        self.ops = ops
    
    def do_eval(self, wires):
        return self.f(*(op.eval(wires) for op in self.ops))

UNARY_OPS = { 'NOT': operator.invert }
BINARY_OPS = { 'AND': operator.and_, 'OR': operator.or_, 'LSHIFT': operator.lshift, 'RSHIFT': operator.rshift }

if __name__ == '__main__':
    import sys
    
    wires = dict()
    for line in sys.stdin:
        m = re.match(
            r'(?P<assign>((?P<a_in_l>[a-z]+)|(?P<a_in_n>[0-9]+)) -> (?P<a_out>[a-z]+))|' +
            r'(?P<unary>(?P<u_op>[A-Z]+) (?P<u_in>[a-z]+) -> (?P<u_out>[a-z]+))|' +
            r'(?P<binary>((?P<b_in1_l>[a-z]+)|(?P<b_in1_n>[0-9]+)) (?P<b_op>[A-Z]+) ' +
                r'((?P<b_in2_l>[a-z]+)|(?P<b_in2_n>[0-9]+)) -> (?P<b_out>[a-z]+))', line)
        assert m != None
        if m.group('assign') != None:
            # Assign a wire/constant:
            if m.group('a_in_l') != None:
                # Assign a wire:
                inp = WireExpr(m.group('a_in_l'))
            else:
                # Assign a constant:
                inp = ConstExpr(int(m.group('a_in_n')))
            wires[m.group('a_out')] = inp
        elif m.group('unary') != None:
            # Assign an unary expression:
            f = UNARY_OPS[m.group('u_op')]
            wires[m.group('u_out')] = FuncExpr(f, WireExpr(m.group('u_in')))
        elif m.group('binary') != None:
            # Assign a binary expression:
            f = BINARY_OPS[m.group('b_op')]
            if m.group('b_in1_l') != None:
                in1 = WireExpr(m.group('b_in1_l'))
            else:
                in1 = ConstExpr(int(m.group('b_in1_n')))
            if m.group('b_in2_l') != None:
                in2 = WireExpr(m.group('b_in2_l'))
            else:
                in2 = ConstExpr(int(m.group('b_in2_n')))
            wires[m.group('b_out')] = FuncExpr(f, in1, in2)
        else:
            assert False
    # DEBUG:
    #for k in wires:
    #    print(k, '=', wires[k].eval(wires) & 0xFFFF)
    print(wires['a'].eval(wires) & 0xFFFF)
