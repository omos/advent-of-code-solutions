#!/usr/bin/env python3

from hashlib import md5

if __name__ == '__main__':
    import sys
    
    for line in sys.stdin:
        found = False
        for i in range(0, 10000000):
            hd = md5((line + str(i)).encode('ascii')).hexdigest()
            if hd.startswith('000000'):
                print(i)
                found = True
                break
        assert found