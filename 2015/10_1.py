#!/usr/bin/env python3

def look_and_say(s):
    digit = '0'
    count = 0
    res = ''
    for c in s:
        if c == digit:
            count += 1
        else:
            if count > 0:
                res += str(count) + digit
            digit = c
            count = 1
    if count > 0:
        res += str(count) + digit
    return res

if __name__ == '__main__':
    import sys
    for line in sys.stdin:
        res = line.strip()
        for i in range(40):
            res = look_and_say(res)
        print(len(res))