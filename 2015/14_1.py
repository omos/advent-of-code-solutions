#!/usr/bin/env python3

import re

def get_deer_state(t, definition):
    m = re.match(r"(\w+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.", definition)
    name, speed, time, rest = m.group(1), int(m.group(2)), int(m.group(3)), int(m.group(4))
    period = time + rest
    n_periods = t // period
    rem = t % period
    
    state = n_periods * speed * time
    state += rem * speed if rem <= time else time * speed
    return state

if __name__ == '__main__':
    import sys
    
    print(max(get_deer_state(2503, line) for line in sys.stdin))