#!/usr/bin/env python3

from itertools import permutations

def get_options(vertices, edges):
    for cycle in permutations(vertices):
        length = 0
        last = cycle[0]
        for i in range(1, len(cycle)):
            current = cycle[i]
            length += edges[frozenset((last, current))]
            last = current
        length += edges[frozenset((last, cycle[0]))]
        yield length

if __name__ == '__main__':
    import sys, re
    from collections import defaultdict
    vertices = set()
    edges = defaultdict(int)
    for line in sys.stdin:
        m = re.match(r"(\w+) would (lose|gain) (\d+) happiness units by sitting next to (\w+)", line)
        src, sign, dist, dst = m.group(1), m.group(2), m.group(3), m.group(4)
        vertices.add(src)
        vertices.add(dst)
        edges[frozenset((src, dst))] += int(dist) * (1 if sign == 'gain' else -1)
    
    print(max(get_options(vertices, edges)))