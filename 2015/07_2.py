#!/usr/bin/env python3

import re
import operator

class Expr(object):
    def __init__(self):
        pass
    
    def eval(self, wires):
        raise NotImplementedError()

class ConstExpr(Expr):
    def __init__(self, val):
        Expr.__init__(self)
        self.val = val
    
    def eval(self, wires):
        return self.val

class WireExpr(Expr):
    def __init__(self, label):
        Expr.__init__(self)
        self.label = label
    
    def eval(self, wires):
        return wires[self.label].get_value(wires)

class FuncExpr(Expr):
    def __init__(self, f, *ops):
        Expr.__init__(self)
        self.f = f
        self.ops = ops
    
    def eval(self, wires):
        return self.f(*(op.eval(wires) for op in self.ops))

class WireEntry(object):
    def __init__(self, expr):
        self.expr = expr
        self.cached_val = None
    
    def reset(self):
        self.cached_val = None
    
    def get_value(self, wires):
        if self.cached_val != None:
            return self.cached_val
        self.cached_val = self.expr.eval(wires)
        return self.cached_val

UNARY_OPS = { 'NOT': operator.invert }
BINARY_OPS = { 'AND': operator.and_, 'OR': operator.or_, 'LSHIFT': operator.lshift, 'RSHIFT': operator.rshift }

if __name__ == '__main__':
    import sys
    
    wires = dict()
    for line in sys.stdin:
        m = re.match(r'(?P<assign>((?P<c_in_l>[a-z]+)|(?P<c_in_n>[0-9]+)) -> (?P<c_out>[a-z]+))|' +
                     r'(?P<unary>(?P<u_op>[A-Z]+) (?P<u_in>[a-z]+) -> (?P<u_out>[a-z]+))|' +
                     r'(?P<binary>((?P<b_in1_l>[a-z]+)|(?P<b_in1_n>[0-9]+)) (?P<b_op>[A-Z]+) ((?P<b_in2_l>[a-z]+)|(?P<b_in2_n>[0-9]+)) -> (?P<b_out>[a-z]+))', line)
        assert m != None
        if m.group('assign') != None:
            if m.group('c_in_l') != None:
                inp = WireExpr(m.group('c_in_l'))
            else:
                inp = ConstExpr(int(m.group('c_in_n')))
            wires[m.group('c_out')] = WireEntry(inp)
        elif m.group('unary') != None:
            f = UNARY_OPS[m.group('u_op')]
            wires[m.group('u_out')] = WireEntry(FuncExpr(f, WireExpr(m.group('u_in'))))
        elif m.group('binary') != None:
            f = BINARY_OPS[m.group('b_op')]
            if m.group('b_in1_l') != None:
                in1 = WireExpr(m.group('b_in1_l'))
            else:
                in1 = ConstExpr(int(m.group('b_in1_n')))
            if m.group('b_in2_l') != None:
                in2 = WireExpr(m.group('b_in2_l'))
            else:
                in2 = ConstExpr(int(m.group('b_in2_n')))
            wires[m.group('b_out')] = WireEntry(FuncExpr(f, in1, in2))
        else:
            assert False
    a_val = wires['a'].get_value(wires)
    print(a_val & 0xFFFF)
    
    wires['b'] = WireEntry(ConstExpr(a_val))
    for k in wires:
        wires[k].reset()
    print(wires['a'].get_value(wires) & 0xFFFF)
