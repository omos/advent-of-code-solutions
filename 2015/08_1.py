#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    print(sum(len(line.strip()) - len(eval(line.strip())) for line in sys.stdin))