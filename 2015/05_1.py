#!/usr/bin/env python3

from collections import Counter
import re

def is_nice(s):
    counter = Counter(s)
    if sum(counter[c] for c in 'aeiou') < 3:
        return False
    if re.search(r'(.)\1', s) == None:
        return False
    if any(sub in s for sub in ('ab', 'cd', 'pq', 'xy')):
        return False
    return True

if __name__ == '__main__':
    import sys
    
    print(sum(is_nice(line) for line in sys.stdin))
