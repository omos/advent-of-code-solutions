#!/usr/bin/env python3

def process_value(obj):
    if isinstance(obj, dict):
        if any(obj[k] == "red" for k in obj):
            print('ignored')
            return 0
        else:
            print('dict')
            return sum(process_value(obj[k]) for k in obj)
    elif isinstance(obj, list):
        print('list')
        return sum(process_value(val) for val in obj)
    elif isinstance(obj, int):
        print('int')
        return obj
    print(type(obj))
    return 0

if __name__ == '__main__':
    import sys
    for line in sys.stdin:
        obj = eval(line) # naughty >:D
        print(process_value(obj))