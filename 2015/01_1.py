#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    floor = 0
    for l in sys.stdin:
        for c in l:
            if c == ')':
                floor -= 1
            elif c == '(':
                floor += 1
    
    print(floor)