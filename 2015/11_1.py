#!/usr/bin/env python3

def increment_pw(pw):
    res = ''
    carry = True
    for i in reversed(range(-8, 0)):
        if carry:
            n = ord(pw[i])
            if n == ord('z'):
                res = 'a' + res
                carry = True
            else:
                carry = False
                res = chr(n + 1) + res
        else:
            res = pw[i] + res
    return res

ALPHABET = "abcdefghjkmnpqrstuvwxyz"

def is_valid(pw):
    if any(c in "ilo" for c in pw):
        return False
    if sum(p * 2 in pw for p in ALPHABET) < 2:
        return False
    if not any((chr(i) + chr(i + 1) + chr(i + 2)) in pw for i in range(ord('a'), ord('y'))):
        return False
    return True

def next_pw(pw):
    while True:
        pw = increment_pw(pw)
        #print(pw)
        if is_valid(pw):
            return pw

if __name__ == '__main__':
    assert not is_valid('hijklmmn')
    assert not is_valid('abbceffg')
    assert not is_valid('abbcegjk')
    #assert next_pw('abcdefgh') == 'abcdffaa'
    #assert next_pw('ghijklmn') == 'ghjaabcc'
    assert is_valid('vzbxxyzz')
    
    import sys
    for line in sys.stdin:
        print(next_pw(line.strip()))