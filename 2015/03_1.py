#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    x = 0
    y = 0
    visited = set()
    visited.add((x, y))
    for l in sys.stdin:
        for c in l:
            if c == '>':
                x += 1
            elif c == '<':
                x -= 1
            elif c == '^':
                y += 1
            elif c == 'v':
                y -= 1
            visited.add((x, y))
    
    print(len(visited))