#!/usr/bin/env python3

from itertools import permutations

def get_lengths(vertices, edges):
    for path in permutations(vertices):
        length = 0
        last = path[0]
        for i in range(1, len(path)):
            current = path[i]
            edge = frozenset((last, current))
            if edge not in edges:
                length = -1
                break
            length += edges[edge]
            last = current
        if length != -1:
            yield length

if __name__ == '__main__':
    import sys, re
    vertices = set()
    edges = dict()
    for line in sys.stdin:
        m = re.match(r"(\w+) to (\w+) = (\d+)", line)
        src, dst, dist = m.group(1), m.group(2), m.group(3)
        vertices.add(src)
        vertices.add(dst)
        edges[frozenset((src, dst))] = int(dist)
    #print(vertices)
    #print(edges)
    
    print(min(get_lengths(vertices, edges)))