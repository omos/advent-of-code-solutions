#!/usr/bin/env python3

def pass_to_id(p):
    row, col = p[:7], p[7:]
    row = int(row.replace('F', '0').replace('B', '1'), base=2)
    col = int(col.replace('L', '0').replace('R', '1'), base=2)
    return row * 8 + col

if __name__ == '__main__':
    import sys

    seats = [False] * (128 * 8)
    for line in sys.stdin:
        seats[pass_to_id(line.strip())] = True
    print(seats.index(False, seats.index(True)))
