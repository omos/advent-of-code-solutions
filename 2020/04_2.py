#!/usr/bin/env python3

import re

RE_HCL = re.compile(r'^#[0-9a-f]{6}$')
RE_PID = re.compile(r'^[0-9]{9}$')
ECL_OPTIONS = frozenset('amb blu brn gry grn hzl oth'.split(' '))

def validate_range(value, min, max):
    try:
        return min <= int(value) <= max
    except ValueError:
        return False

def validate_byr(value):
    return validate_range(value, 1920, 2002)

def validate_iyr(value):
    return validate_range(value, 2010, 2020)

def validate_eyr(value):
    return validate_range(value, 2020, 2030)

def validate_hgt(value):
    if value.endswith('cm'):
        return validate_range(value[:-2], 150, 193)
    elif value.endswith('in'):
        return validate_range(value[:-2], 59, 76)
    return False

def validate_hcl(value):
    return RE_HCL.match(value) != None

def validate_ecl(value):
    return value in ECL_OPTIONS

def validate_pid(value):
    return RE_PID.match(value)

REQUIRED_FIELDS = {
    'byr': validate_byr,
    'iyr': validate_iyr,
    'eyr': validate_eyr,
    'hgt': validate_hgt,
    'hcl': validate_hcl,
    'ecl': validate_ecl,
    'pid': validate_pid,
    }

if __name__ == '__main__':
    import sys

    total = 0
    fields = set(REQUIRED_FIELDS)
    for line in sys.stdin:
        line = line.strip()
        if len(line) == 0:
            total += not fields
            fields = set(REQUIRED_FIELDS)
        else:
            fields.difference_update(k
                                     for f in line.split(' ')
                                     for k, v in [f.split(':')]
                                     if k in REQUIRED_FIELDS
                                     if REQUIRED_FIELDS[k](v))
    total += not fields
    print(total)
