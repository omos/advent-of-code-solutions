#!/usr/bin/env python3

def solve(base, target):
    for i in reversed(range(len(base) - 1)):
        total = base[i]
        for k in range(i + 1, len(base)):
            total += base[k]
            if total == target:
                return min(base[i:k + 1]) + max(base[i:k + 1])
            elif total > target:
                break
    return None

if __name__ == '__main__':
    import sys

    invalid = None
    base = []
    for line in sys.stdin:
        n = int(line.strip())
        if len(base) >= 25:
            if all(n != x + y for x in base[-25:] for y in base[-25:]):
                invalid = n
                break
        base.append(n)

    print(solve(base, invalid))
