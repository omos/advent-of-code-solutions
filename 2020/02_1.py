#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    res = 0
    for line in sys.stdin:
        rng, c, password = line.strip().split(' ')
        s, e = map(int, rng.split('-'))
        c = c.rstrip(':')
        if s <= password.count(c) <= e:
            res += 1
    print(res)
