#!/usr/bin/env python3

from collections import defaultdict
from itertools import chain

if __name__ == '__main__':
    import sys

    parents = defaultdict(list)
    for line in sys.stdin:
        l, r = line.strip().split(' bags contain ')
        if r == 'no other bags.':
            continue
        for item in r.rstrip('.').split(', '):
            count, adj, col, _ = item.split(' ')
            parents[adj + ' ' + col].append(l)

    roots = set()
    cursors = set(['shiny gold'])
    while len(cursors):
        cursors = set(chain.from_iterable(parents[k] for k in cursors))
        cursors.difference_update(roots)
        roots.update(cursors)
    print(len(roots))
