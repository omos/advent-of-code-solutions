#!/usr/bin/env python3

def count_trees(pattern, right, down):
    h = len(pattern)
    w = len(pattern[0])
    res = 0
    for i in range(1, (h + down - 1) // down):
        r = i * down
        c = i * right
        if pattern[r][c % w]:
            res += 1
    return res

if __name__ == '__main__':
    import sys

    pattern = [[c == '#' for c in line.strip()] for line in sys.stdin]
    res = 1
    for right, down in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
        res *= count_trees(pattern, right, down)
    print(res)
