#!/usr/bin/env python3

def count_trees(pattern, right, down):
    h = len(pattern)
    w = len(pattern[0])
    res = 0
    for i in range(1, h // down):
        r = i * down
        c = i * right
        if pattern[r][c % w]:
            res += 1
    return res

if __name__ == '__main__':
    import sys

    pattern = [[c == '#' for c in line.strip()] for line in sys.stdin]
    print(count_trees(pattern, 3, 1))
