#!/usr/bin/env python3

REQUIRED_FIELDS = ('byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid')

if __name__ == '__main__':
    import sys

    total = 0
    fields = set(REQUIRED_FIELDS)
    for line in sys.stdin:
        line = line.strip()
        if len(line) == 0:
            total += not fields
            fields = set(REQUIRED_FIELDS)
        else:
            fields.difference_update(f.split(':')[0] for f in line.split(' '))
    total += not fields
    print(total)
