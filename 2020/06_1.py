#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    total = 0
    ticked = set()
    for line in sys.stdin:
        line = line.strip()
        if len(line) == 0:
            total += len(ticked)
            ticked.clear()
        else:
            ticked.update(line)
    total += len(ticked)
    print(total)
    
