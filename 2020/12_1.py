#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    x = 0
    y = 0
    dx = 1
    dy = 0
    for line in sys.stdin:
        line = line.strip()
        instr = line[0]
        arg = int(line[1:])
        if instr == 'F':
            x += dx * arg
            y += dy * arg
        elif instr == 'N':
            y += arg
        elif instr == 'E':
            x += arg
        elif instr == 'S':
            y -= arg
        elif instr == 'W':
            x -= arg
        elif instr in 'LR':
            angle = arg // 90
            if instr == 'L':
                angle = (4 - angle) % 4
            if angle == 1:
                dx, dy = dy, -dx
            elif angle == 2:
                dx, dy = -dx, -dy
            elif angle == 3:
                dx, dy = -dy, dx
    print(abs(x) + abs(y))
