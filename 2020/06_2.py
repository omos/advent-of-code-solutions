#!/usr/bin/env python3

ALPHABET = [chr(c) for c in range(ord('a'), ord('z') + 1)]

if __name__ == '__main__':
    import sys

    total = 0
    ticked = set(ALPHABET)
    for line in sys.stdin:
        line = line.strip()
        if len(line) == 0:
            total += len(ticked)
            ticked = set(ALPHABET)
        else:
            ticked.intersection_update(line)
    total += len(ticked)
    print(total)
    
