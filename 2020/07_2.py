#!/usr/bin/env python3

def count_bags(rules, root):
    total = 0
    for count, color in rules[root]:
        total += count * (1 + count_bags(rules, color))
    return total

if __name__ == '__main__':
    import sys

    rules = {}
    for line in sys.stdin:
        l, r = line.strip().split(' bags contain ')
        if r == 'no other bags.':
            r = []
        else:
            r = [(int(count), ' '.join((adj, col)))
                 for item in r.rstrip('.').split(', ')
                 for count, adj, col, _ in [item.split(' ')]]
        rules[l] = r

    print(count_bags(rules, 'shiny gold'))
