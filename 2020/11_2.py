#!/usr/bin/env python3

def cast_ray(r, c, dr, dc, w, h):
    r += dr
    c += dc
    while 0 <= r < h and 0 <= c < w:
        yield (r, c)
        r += dr
        c += dc

def adjacent(r, c, w, h):
    for dr in (-1, 0, 1):
        for dc in (-1, 0, 1):
            if (dr, dc) != (0, 0):
                yield cast_ray(r, c, dr, dc, w, h)

def find_first_seen(state, ray):
    for pos in ray:
        if pos in state:
            return state[pos]
    return False

def sum_adjacent(state, r, c, w, h):
    return sum(find_first_seen(state, ray) for ray in adjacent(r, c, w, h))

def dump_state(state, w, h):
    for r in range(h):
        print(''.join('L#'[state[(r, c)]] if (r, c) in state else '.' for c in range(w)))
    print()

if __name__ == '__main__':
    import sys

    layout = [line.strip() for line in sys.stdin]
    w = len(layout[0])
    h = len(layout)

    state = dict(((r, c), False)
                 for r in range(h)
                 for c in range(w)
                 if layout[r][c] != '.')
    while True:
        new_state = dict(((r, c), sum_adjacent(state, r, c, w, h) < (5 if state[(r, c)] else 1))
                         for r, c in state)
        if new_state == state:
            break
        state = new_state
    print(sum(state.values()))
