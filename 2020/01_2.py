#!/usr/bin/env python3

from itertools import combinations

def find(numbers):
    numbers = set(numbers)
    for i, j in combinations(numbers, 2):
        key = 2020 - i - j
        if key in numbers:
            return i * j * key
    assert False

if __name__ == '__main__':
    import sys

    print(find(int(line.strip()) for line in sys.stdin))
