#!/usr/bin/env python3

def adjacent(r, c):
    for dr in (-1, 0, 1):
        for dc in (-1, 0, 1):
            if (dr, dc) != (0, 0):
                yield (r + dr, c + dc)

def sum_adjacent(state, r, c):
    return sum(state[pos] for pos in adjacent(r, c) if pos in state)

def dump_state(state, w, h):
    for r in range(h):
        print(''.join('L#'[state[(r, c)]] if (r, c) in state else '.' for c in range(w)))
    print()

if __name__ == '__main__':
    import sys

    layout = [line.strip() for line in sys.stdin]
    w = len(layout[0])
    h = len(layout)

    state = dict(((r, c), False)
                 for r in range(h)
                 for c in range(w)
                 if layout[r][c] != '.')
    while True:
        new_state = dict(((r, c), sum_adjacent(state, r, c) < (4 if state[(r, c)] else 1))
                         for r, c in state)
        if new_state == state:
            break
        state = new_state
    print(sum(state.values()))
