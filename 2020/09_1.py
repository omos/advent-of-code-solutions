#!/usr/bin/env python3

from collections import deque

if __name__ == '__main__':
    import sys

    base = deque(maxlen=25)
    for line in sys.stdin:
        n = int(line.strip())
        if len(base) == base.maxlen:
            if all(n != x + y for x in base for y in base):
                print(n)
                break
        base.append(n)
