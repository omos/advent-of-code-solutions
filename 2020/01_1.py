#!/usr/bin/env python3

def find(numbers):
    numbers = set(numbers)
    while len(numbers):
        n = numbers.pop()
        if (2020 - n) in numbers:
            return n * (2020 - n)
    assert False

if __name__ == '__main__':
    import sys

    print(find(int(line.strip()) for line in sys.stdin))
