#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    program = []
    for line in sys.stdin:
        insn, arg = line.strip().split(' ')
        program.append((insn, int(arg)))

    pos = 0
    acc = 0
    visited = [False] * len(program)
    while not visited[pos]:
        visited[pos] = True
        insn, arg = program[pos]
        if insn == 'acc':
            acc += arg
            pos += 1
        elif insn == 'jmp':
            pos += arg
        else:
            pos += 1
    print(acc)
