#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    res = 0
    for line in sys.stdin:
        rng, c, password = line.strip().split(' ')
        c = c.rstrip(':')
        if sum(password[int(pos) - 1] == c for pos in rng.split('-')) == 1:
            res += 1
    print(res)
