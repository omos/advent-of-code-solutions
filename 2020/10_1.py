#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    adapters = [int(line.strip()) for line in sys.stdin]
    adapters.sort()
    total1 = 0
    total3 = 1
    last = 0
    for a in adapters:
        diff = a - last
        if diff == 1:
            total1 += 1
        elif diff == 3:
            total3 += 1
        last = a
    print(total1 * total3)
