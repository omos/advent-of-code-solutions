#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    x = 0
    y = 0
    dx = 10
    dy = 1
    for line in sys.stdin:
        line = line.strip()
        instr = line[0]
        arg = int(line[1:])
        if instr == 'F':
            x += dx * arg
            y += dy * arg
        elif instr == 'N':
            dy += arg
        elif instr == 'E':
            dx += arg
        elif instr == 'S':
            dy -= arg
        elif instr == 'W':
            dx -= arg
        elif instr in 'LR':
            angle = arg // 90
            if instr == 'L':
                angle = (4 - angle) % 4
            if angle == 1:
                dx, dy = dy, -dx
            elif angle == 2:
                dx, dy = -dx, -dy
            elif angle == 3:
                dx, dy = -dy, dx
    print(abs(x) + abs(y))
