#!/usr/bin/env python3

def run_program(program):
    pos = 0
    acc = 0
    visited = [False] * len(program)
    while not visited[pos]:
        visited[pos] = True
        insn, arg = program[pos]
        if insn == 'acc':
            acc += arg
            pos += 1
        elif insn == 'jmp':
            pos += arg
        else:
            pos += 1
        if pos >= len(program):
            return acc
    return None

if __name__ == '__main__':
    import sys

    program = []
    for line in sys.stdin:
        insn, arg = line.strip().split(' ')
        program.append((insn, int(arg)))

    for pos in range(len(program)):
        insn, arg = program[pos]
        if insn == 'jmp':
            program[pos] = ('nop', arg)
        elif insn == 'nop':
            program[pos] = ('jmp', arg)
        else:
            continue
        res = run_program(program)
        if res != None:
            print(res)
            break
        program[pos] = (insn, arg)
