#!/usr/bin/env python3

if __name__ == '__main__':
    import sys

    adapters = [int(line.strip()) for line in sys.stdin]
    adapters.sort()

    paths = dict((a, 0) for a in adapters)
    paths[0] = 1
    for i in [0] + adapters:
        n = paths[i]
        for k in (1, 2, 3):
            if (i + k) in paths:
                paths[i + k] += n
    print(paths[adapters[-1]])
