#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    labels = dict()
    
    for line in sys.stdin:
        src, dsts = line.strip().split('<->')
        dsts = dsts.split(', ')
        
        src = int(src)
        
        src_label = src
        if src not in labels:
            labels[src] = src_label
        else:
            src_label = labels[src]
        
        dst_labels = set(labels[dst] for dst in map(int, dsts) if dst in labels)
        
        for k in labels:
            if labels[k] in dst_labels:
                labels[k] = src_label
    
    print(sum(1 for p in labels if labels[p] == labels[0]))
