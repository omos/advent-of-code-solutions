#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    total = 0
    first = None
    last = None
    for c in sys.stdin.read().strip():
        if first == None:
            first = c
        elif c == last:
            total += int(c)
        last = c
    if last == first:
        total += int(first)
    print(total)
