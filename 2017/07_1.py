#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    nodes = set()
    inner = set()
    
    for line in sys.stdin:
        sides = line.strip().split(' -> ')
        children = [] if len(sides) < 2 else sides[1].split(', ')
        name, weight = sides[0].split(' ')
        weight = int(weight.strip('()'))
        
        nodes.add(name)
        inner.update(children)
    
    print(nodes - inner)
