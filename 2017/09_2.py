#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    ngarbage = 0
    for line in sys.stdin:
        cancel = False
        garbage = False
        for c in line.strip():
            if cancel:
                cancel = False
            elif c == '!':
                cancel = True
            elif garbage:
                if c != '>':
                    ngarbage += 1
                else:
                    garbage = False
            elif c == '<':
                garbage = True
    print(ngarbage)
