#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from collections import defaultdict
    
    values = defaultdict(int)
    max_value = 0
    
    for line in sys.stdin:
        mod_reg, mod_op, mod_val, _, cmp_reg, cmp_op, cmp_val = line.strip().split()
        
        mod_val = int(mod_val)
        cmp_val = int(cmp_val)
        
        cmp_reg_val = values[cmp_reg]
        condition = None
        if cmp_op == '==':
            condition = cmp_reg_val == cmp_val
        elif cmp_op == '!=':
            condition = cmp_reg_val != cmp_val
        elif cmp_op == '<':
            condition = cmp_reg_val < cmp_val
        elif cmp_op == '>':
            condition = cmp_reg_val > cmp_val
        elif cmp_op == '<=':
            condition = cmp_reg_val <= cmp_val
        elif cmp_op == '>=':
            condition = cmp_reg_val >= cmp_val
        assert condition != None
        
        if condition:
            if mod_op == 'inc':
                values[mod_reg] += mod_val
            elif mod_op == 'dec':
                values[mod_reg] -= mod_val
        
        max_value = max(max_value, max(values.values()))
    
    print(max_value)
