#!/usr/bin/env python3

def run_generator(start, factor, divisor):
    val = start
    while True:
        val *= factor
        val %= 0x7FFFFFFF
        if val % divisor == 0:
            yield val


if __name__ == '__main__':
    import sys
    
    factor_a = 16807
    factor_b = 48271
    
    start_a = int(sys.stdin.readline().strip().split(' ')[-1])
    start_b = int(sys.stdin.readline().strip().split(' ')[-1])
    
    a = run_generator(start_a, factor_a, 4)
    b = run_generator(start_b, factor_b, 8)
    
    count = 0
    for i in range(5*1000*1000):
        count += (next(a) & 0xFFFF) == (next(b) & 0xFFFF)
    
    print(count)
