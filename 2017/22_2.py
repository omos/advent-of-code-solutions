#!/usr/bin/env python3

BURSTS = 10000000

DIR_UP = 0
DIR_RIGHT = 1
DIR_DOWN = 2
DIR_LEFT = 3

ROTATION_NONE = 0
ROTATION_RIGHT = 1
ROTATION_REVERSE = 2
ROTATION_LEFT = 3

def dir_rotate(direction, rotation):
    return (direction + rotation) % 4

DIR_TO_DELTA = [(-1, 0), (0, 1), (1, 0), (0, -1)]

STATE_CLEAN = 0
STATE_WEAKENED = 1
STATE_INFECTED = 2
STATE_FLAGGED = 3

STATE_TO_ROTATION = [ROTATION_LEFT, ROTATION_NONE, ROTATION_RIGHT, ROTATION_REVERSE]

def state_next(state):
    return (state + 1) % 4

if __name__ == '__main__':
    import sys
    from collections import defaultdict
    
    states = defaultdict(int)
    
    r = 0
    for line in sys.stdin:
        row = line.strip()
        offset = len(row) // 2
        for c in range(len(row)):
            if row[c] == '#':
                states[(r - offset, c - offset)] = STATE_INFECTED
            c += 1
        r += 1
    
    r, c = 0, 0
    count = 0
    direction = DIR_UP
    for i in range(BURSTS):
        state = states[(r, c)]
        rotation = STATE_TO_ROTATION[state]
        direction = dir_rotate(direction, rotation)
        
        state = state_next(state)
        if state == STATE_INFECTED:
            count += 1
        states[(r, c)] = state
        
        dr, dc = DIR_TO_DELTA[direction]
        r += dr
        c += dc
    
    print(count)
