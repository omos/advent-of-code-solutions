#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    banks = list(map(int, sys.stdin.readline().strip().split()))
    history = set()
    count = 0
    while tuple(banks) not in history:
        history.add(tuple(banks))
        
        pos, value = max(((i, banks[i]) for i in range(len(banks))), key=(lambda x: x[1]))
        banks[pos] = 0
        while value > 0:
            pos = (pos + 1) % len(banks)
            banks[pos] += 1
            value -= 1
        
        count += 1
    
    print(count)
