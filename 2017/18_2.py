#!/usr/bin/env python3

class Program(object):
    def __init__(self, commands, pid):
        self.commands = commands
        self.pos = 0
        self.regs = dict((chr(ord('a') + i), 0) for i in range(26))
        self.regs['p'] = pid
        self.messages = deque()
        self.waiting = False
        self.counter = 0
    
    def is_terminated(self):
        return self.pos < 0 or self.pos >= len(self.commands)
    
    def is_waiting(self):
        return self.waiting and len(self.messages) == 0
    
    def send_message(self, value):
        self.messages.appendleft(value)
    
    def read_value(self, value):
        if value in self.regs:
            return self.regs[value]
        return int(value)
    
    def step(self, other):
        cmd = self.commands[self.pos]
        if False:
            pass
        elif cmd[0] == 'rcv':
            if len(self.messages):
                self.regs[cmd[1]] = self.messages.pop()
                self.waiting = False
            else:
                self.waiting = True
                return
        elif cmd[0] == 'snd':
            other.send_message(self.read_value(cmd[1]))
            self.counter += 1
        elif cmd[0] == 'set':
            self.regs[cmd[1]] = self.read_value(cmd[2])
        elif cmd[0] == 'add':
            self.regs[cmd[1]] += self.read_value(cmd[2])
        elif cmd[0] == 'mul':
            self.regs[cmd[1]] *= self.read_value(cmd[2])
        elif cmd[0] == 'mod':
            self.regs[cmd[1]] %= self.read_value(cmd[2])
        elif cmd[0] == 'jgz':
            if self.read_value(cmd[1]) > 0:
                self.pos += self.read_value(cmd[2])
                return
        
        self.pos += 1

if __name__ == '__main__':
    import sys
    from collections import deque
    
    commands = [line.strip().split(' ') for line in sys.stdin]
    
    pid = 0
    programs = [Program(commands, i) for i in range(2)]
    progress = True
    while progress:
        progress = False
        for pid in range(2):
            p, other = programs[pid], programs[1 - pid]
            if p.is_terminated() or p.is_waiting():
                continue
            p.step(other)
            progress = True
            break
    
    print(programs[1].counter)
