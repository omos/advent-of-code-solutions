#!/usr/bin/env python3

def best_bridge(comps, port, length, strength):
    best_length = -1
    best_strength = -1
    for i in range(len(comps)):
        c_strength = sum(comps[i])
        c = list(comps[i])
        if not port in c:
            continue
        del c[c.index(port)]
        sub_port = c[0]
        
        sub_comps = list(comps)
        del sub_comps[i]
        
        sub_length = length + 1
        sub_strength = strength + c_strength
        sub_length, sub_strength = best_bridge(sub_comps, sub_port, sub_length, sub_strength)
        if sub_length > best_length:
            best_length = sub_length
            best_strength = sub_strength
        elif sub_length == best_length and sub_strength > best_strength:
            best_strength = sub_strength
    return (best_length, best_strength) if best_length >= 0 else (length, strength)

if __name__ == '__main__':
    import sys
    
    comps = list(tuple(map(int, p.strip().split('/'))) for p in sys.stdin)
    
    print(best_bridge(comps, 0, 0, 0)[1])
