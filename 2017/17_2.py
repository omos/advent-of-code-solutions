#!/usr/bin/env python3

ITERATIONS = 50 * 1000 * 1000

if __name__ == '__main__':
    import sys
    
    for line in sys.stdin:
        steps = int(line.strip())
        
        after_zero = None
        size = 1
        pos = 0
        for i in range(1, ITERATIONS + 1):
            pos += steps
            pos %= size
            pos += 1
            size += 1
            if pos == 1:
                after_zero = i
        
        print(after_zero)
