#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    severity = 0
    for line in sys.stdin:
        d, r = map(int, line.strip().split(': '))
        
        r1 = r - 1
        if r1 - abs(d % (2 * r1) - r1) == 0:
            severity += d * r
    
    print(severity)
