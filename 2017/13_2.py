#!/usr/bin/env python3

def not_caught(t, r):
    r -= 1
    return r - abs(t % (2 * r) - r) != 0

if __name__ == '__main__':
    import sys
    
    ranges = [tuple(map(int, line.strip().split(': '))) for line in sys.stdin]
    
    delay = 0
    while not all(not_caught(delay + d, r) for d, r in ranges):
        delay += 1
    
    print(delay)
