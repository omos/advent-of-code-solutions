#!/usr/bin/env python3

ITERATIONS = 2017

if __name__ == '__main__':
    import sys
    
    for line in sys.stdin:
        steps = int(line.strip())
        
        buf = [0]
        pos = 0
        for i in range(1, ITERATIONS + 1):
            pos += steps
            pos %= len(buf)
            pos += 1
            buf[pos:pos] = [i]
        
        print(buf[pos + 1])
