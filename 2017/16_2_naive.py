#!/usr/bin/env python3

ITERATIONS = 10 ** 9

PROGRAM_NAMES = list('abcdefghijklmnop')
PROGRAM_COUNT = len(PROGRAM_NAMES)

if __name__ == '__main__':
    import sys
    
    for line in sys.stdin:
        idx_perm = list(range(PROGRAM_COUNT))
        prg_perm = list(range(PROGRAM_COUNT))
        for move in line.strip().split(','):
            kind, args = move[0], move[1:]
            if kind == 's':
                n = int(args)
                for i in range(PROGRAM_COUNT):
                    idx_perm[i] = (idx_perm[i] + n) % PROGRAM_COUNT
            elif kind == 'x':
                a, b = map(int, args.split('/'))
                for i in range(PROGRAM_COUNT):
                    if idx_perm[i] == a:
                        idx_perm[i] = b
                    elif idx_perm[i] == b:
                        idx_perm[i] = a
            elif kind == 'p':
                a, b = map(PROGRAM_NAMES.index, args.split('/'))
                tmp = prg_perm[a]
                prg_perm[a] = prg_perm[b]
                prg_perm[b] = tmp
            else:
                assert False
        
        positions = list(range(PROGRAM_COUNT))
        for i in range(ITERATIONS):
            positions = [idx_perm[positions[prg_perm[k]]] for k in range(PROGRAM_COUNT)]
        
        print(''.join(PROGRAM_NAMES[p] for i in range(PROGRAM_COUNT) for p in range(PROGRAM_COUNT) if positions[p] == i))
