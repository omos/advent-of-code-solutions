#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    checksum = 0
    for line in sys.stdin:
        row = list(map(int, line.split()))
        checksum += max(row) - min(row)
    print(checksum)
