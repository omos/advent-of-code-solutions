#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    for line in sys.stdin:
        programs = list('abcdefghijklmnop')
        
        for move in line.strip().split(','):
            kind, args = move[0], move[1:]
            if kind == 's':
                n = int(args)
                programs = programs[-n:] + programs[:-n]
            elif kind == 'x':
                a, b = map(int, args.split('/'))
                programs[a], programs[b] = programs[b], programs[a]
            elif kind == 'p':
                a, b = map(programs.index, args.split('/'))
                programs[a], programs[b] = programs[b], programs[a]
            else:
                assert False
        
        print(''.join(programs))
