#!/usr/bin/env python3

from math import sqrt

def parse_vec(s):
    return list(map(int, s.split('=')[1].strip('<>').split(',')))

def compute_collision(part0, part1):
    p0, v0, a0 = part0
    p1, v1, a1 = part1
    
    times = []
    for d in range(3):
        a = (a1[d] - a0[d]) / 2
        b = v1[d] - v0[d] + (a1[d] - a0[d]) / 2
        c = p1[d] - p0[d]
        
        if a == 0:
            if b == 0:
                ts = [0] if c == 0 else []
            else:
                ts = [-c / b]
        else:
            discr = b * b - 4 * a * c
            if discr < 0:
                ts = []
            else:
                ts = [(-b + k * sqrt(discr)) / (2 * a) for k in (-1, 1)]
        
        ts = frozenset(int(t) for t in ts if t == int(t))
        
        times.append(ts)
    
    times = times[0] & times[1] & times[2]
    
    return min(times) if len(times) else None

if __name__ == '__main__':
    import sys
    
    from itertools import combinations
    from collections import defaultdict
    
    particles = [[parse_vec(vec) for vec in line.strip().split(', ')] for line in sys.stdin]
    
    particle_count = len(particles)
    
    times = []
    collisions = defaultdict(list)
    for i, k in combinations(range(particle_count), 2):
        t = compute_collision(particles[i], particles[k])
        if t == None:
            continue
        
        times.append(t)
        collisions[t].append((i, k))
    
    times.sort()
    
    particles = set(range(particle_count))
    for t in times:
        remove = set()
        for i, k in collisions[t]:
            if i in particles and k in particles:
                remove.update((i, k))
        particles.difference_update(remove)
    
    print(len(particles))
