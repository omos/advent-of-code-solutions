#!/usr/bin/env python3

def strongest_bridge(comps, port, strength):
    best = -1
    for i in range(len(comps)):
        c_strength = sum(comps[i])
        c = list(comps[i])
        if not port in c:
            continue
        del c[c.index(port)]
        new_comps = list(comps)
        del new_comps[i]
        sub_strength = strongest_bridge(new_comps, c[0], strength + c_strength)
        if best < sub_strength:
            best = sub_strength
    return best if best >= 0 else strength

if __name__ == '__main__':
    import sys
    
    comps = list(tuple(map(int, p.strip().split('/'))) for p in sys.stdin)
    
    print(strongest_bridge(comps, 0, 0))
