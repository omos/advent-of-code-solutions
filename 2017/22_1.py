#!/usr/bin/env python3

BURSTS = 10000

DIR_UP = 0
DIR_RIGHT = 1
DIR_DOWN = 2
DIR_LEFT = 3

def dir_rot_left(direction):
    return (direction - 1) % 4

def dir_rot_right(direction):
    return (direction + 1) % 4

DIR_TO_DELTA = [(-1, 0), (0, 1), (1, 0), (0, -1)]

if __name__ == '__main__':
    import sys
    
    infected = set()
    
    r = 0
    for line in sys.stdin:
        row = line.strip()
        offset = len(row) // 2
        for c in range(len(row)):
            if row[c] == '#':
                infected.add((r - offset, c - offset))
            c += 1
        r += 1
    
    r, c = 0, 0
    count = 0
    direction = DIR_UP
    for i in range(BURSTS):
        if (r, c) in infected:
            direction = dir_rot_right(direction)
            infected.remove((r, c))
        else:
            direction = dir_rot_left(direction)
            infected.add((r, c))
            count += 1
        dr, dc = DIR_TO_DELTA[direction]
        r += dr
        c += dc
    
    print(count)
