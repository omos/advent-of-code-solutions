#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    offsets = [int(line.strip()) for line in sys.stdin]
    steps = 0
    pos = 0
    while pos >= 0 and pos < len(offsets):
        delta = offsets[pos]
        offsets[pos] += 1
        
        pos += delta
        steps += 1
    
    print(steps)
