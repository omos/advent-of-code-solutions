#!/usr/bin/env python3

DATA_LEN = 256
DENSE_HASH_LEN = 16

def compute_knot_hash(s):
    lengths = list(map(ord, s))
    lengths += [17, 31, 73, 47, 23]
    
    data = list(range(DATA_LEN))
    pos = 0
    skip_size = 0
    for r in range(64):
        for length in lengths:
            for i in range(length // 2):
                a = (pos + i) % DATA_LEN
                b = (pos + length - i - 1) % DATA_LEN
                data[a], data[b] = data[b], data[a]
            pos += length + skip_size
            skip_size += 1
    
    dense_hash = 0
    for i in range(DENSE_HASH_LEN):
        for k in range(DATA_LEN // DENSE_HASH_LEN):
            dense_hash ^= data[i * (DATA_LEN // DENSE_HASH_LEN) + k] << (8 * (DENSE_HASH_LEN - i - 1))
    
    return dense_hash

if __name__ == '__main__':
    import sys
    
    key = sys.stdin.readline().strip()
    
    count = 0
    for i in range(128):
        hash_in = '{0}-{1}'.format(key, i)
        
        h = compute_knot_hash(hash_in)
        print('{0:0128b}'.format(h))
        
        count += sum(c == '1' for c in '{0:0128b}'.format(h))
    
    print(count)
