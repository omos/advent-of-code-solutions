#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    commands = [line.strip().split(' ') for line in sys.stdin]
    
    pos = 0
    regs = dict((chr(ord('a') + i), 0) for i in range(26))
    regs['a'] = 1
    
    def read_value(val):
        if val in regs:
            return regs[val]
        return int(val)
    
    while pos >= 0 and pos < len(commands):
        cmd = commands[pos]
        if False:
            pass
        elif cmd[0] == 'set':
            regs[cmd[1]] = read_value(cmd[2])
        elif cmd[0] == 'add':
            regs[cmd[1]] += read_value(cmd[2])
        elif cmd[0] == 'sub':
            regs[cmd[1]] -= read_value(cmd[2])
        elif cmd[0] == 'mul':
            regs[cmd[1]] *= read_value(cmd[2])
        elif cmd[0] == 'jnz':
            if read_value(cmd[1]) != 0:
                pos += read_value(cmd[2])
                continue
        
        print(regs)
        
        pos += 1
    
    print(regs['h'])
