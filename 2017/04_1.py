#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    valid = 0
    for line in sys.stdin:
        words = line.strip().split(' ')
        if len(set(words)) == len(words):
            valid += 1
    
    print(valid)
