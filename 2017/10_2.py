#!/usr/bin/env python3

DATA_LEN = 256
DENSE_HASH_LEN = 16

if __name__ == '__main__':
    import sys
    
    lengths = list(map(ord, sys.stdin.readline().strip()))
    lengths += [17, 31, 73, 47, 23]
    
    data = list(range(DATA_LEN))
    pos = 0
    skip_size = 0
    for r in range(64):
        for length in lengths:
            for i in range(length // 2):
                a = (pos + i) % DATA_LEN
                b = (pos + length - i - 1) % DATA_LEN
                data[a], data[b] = data[b], data[a]
            pos += length + skip_size
            skip_size += 1
    
    dense_hash = [0] * DENSE_HASH_LEN
    for i in range(DENSE_HASH_LEN):
        for k in range(DATA_LEN // DENSE_HASH_LEN):
            dense_hash[i] ^= data[i * (DATA_LEN // DENSE_HASH_LEN) + k]
    
    print(''.join('{0:02x}'.format(x) for x in dense_hash))
