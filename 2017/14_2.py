#!/usr/bin/env python3

DATA_LEN = 256
DENSE_HASH_LEN = 16

def compute_knot_hash(s):
    lengths = list(map(ord, s))
    lengths += [17, 31, 73, 47, 23]
    
    data = list(range(DATA_LEN))
    pos = 0
    skip_size = 0
    for r in range(64):
        for length in lengths:
            for i in range(length // 2):
                a = (pos + i) % DATA_LEN
                b = (pos + length - i - 1) % DATA_LEN
                data[a], data[b] = data[b], data[a]
            pos += length + skip_size
            skip_size += 1
    
    dense_hash = 0
    for i in range(DENSE_HASH_LEN):
        for k in range(DATA_LEN // DENSE_HASH_LEN):
            dense_hash ^= data[i * (DATA_LEN // DENSE_HASH_LEN) + k] << (8 * (DENSE_HASH_LEN - i - 1))
    
    return dense_hash

def range_mask(start, end):
    return ((1 << end) - 1) & ~((1 << start) - 1)

def get_regions(h):
    start = None
    for i in range(128):
        if h & (1 << i):
            if start == None:
                start = i
        elif start != None:
            yield range_mask(start, i)
            start = None
    if start != None:
        yield range_mask(start, 128)

if __name__ == '__main__':
    import sys
    
    key = sys.stdin.readline().strip()
    
    count = 0
    regions = []
    for i in range(128):
        h = compute_knot_hash('{0}-{1}'.format(key, i))
        
        count += sum((r & h) == 0 for r in regions)
        
        new_regions = list(get_regions(h))
        
        i = 0
        while i < len(new_regions):
            k = i + 1
            while k < len(new_regions):
                for old_r in regions:
                    if new_regions[i] & old_r and new_regions[k] & old_r:
                        new_regions[i] |= new_regions[k]
                        del new_regions[k]
                        k -= 1
                        break
                k += 1
            i += 1
        
        regions = new_regions
    
    count += len(regions)
    
    print(count)
