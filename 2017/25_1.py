#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from collections import deque
    
    initial_state = sys.stdin.readline().strip().strip('.').split(' ')[-1]
    checksum_steps = int(sys.stdin.readline().strip().strip('.').split(' ')[-2])
    
    rules = {}
    
    while True:
        line = sys.stdin.readline()
        if len(line) == 0:
            break
        
        state = sys.stdin.readline().strip().strip(':').split(' ')[-1]
        rule = [None, None]
        for i in range(2):
            value = int(sys.stdin.readline().strip().strip(':').split(' ')[-1])
            new_value = int(sys.stdin.readline().strip().strip('.').split(' ')[-1])
            direction = sys.stdin.readline().strip().strip('.').split(' ')[-1]
            new_state = sys.stdin.readline().strip().strip('.').split(' ')[-1]
            
            direction = 1 if direction == 'right' else -1
            rule[value] = (new_value, direction, new_state)
        
        rules[state] = rule
    
    state = initial_state
    pos = 0
    tape = set()
    for i in range(checksum_steps):
        new_value, direction, new_state = rules[state][pos in tape]
        if new_value:
            tape.add(pos)
        else:
            tape.discard(pos)
        pos += direction
        state = new_state
    
    print(len(tape))
