#!/usr/bin/env python3

ITERATIONS = 5

PATTERN_SIZE = { 2: 3, 3: 4 }

def pattern_to_key_0(p, r0, c0, range1, range2):
    key = 0
    for r in range1:
        for c in range2:
            key <<= 1
            key |= p[r0 + r][c0 + c] == '#'
    return key

def pattern_to_key_1(p, r0, c0, range1, range2):
    key = 0
    for c in range1:
        for r in range2:
            key <<= 1
            key |= p[r0 + r][c0 + c] == '#'
    return key

def pattern_to_keys(p, r0, c0, n):
    ranges = (range(n), range(n - 1, -1, -1))
    for r1 in ranges:
        for r2 in ranges:
            yield pattern_to_key_0(p, r0, c0, r1, r2)
            yield pattern_to_key_1(p, r0, c0, r1, r2)

def apply_pattern(image, r0, c0, n, pattern):
    for r in range(n):
        for c in range(n):
            image[r0 + r][c0 + c] = pattern[r][c]

if __name__ == '__main__':
    import sys
    
    patterns = { 2: [None] * (1 << 4), 3: [None] * (1 << 9) }
    
    for line in sys.stdin:
        l, r = (p.split('/') for p in line.strip().split(' => '))
        
        for k in pattern_to_keys(l, 0, 0, len(l)):
            if patterns[len(l)][k] != None and patterns[len(l)][k] != r:
                print(patterns[len(l)][k], l)
            patterns[len(l)][k] = r
    
    image = [['.', '#', '.'], ['.', '.', '#'], ['#', '#', '#']]
    size = 3
    for i in range(ITERATIONS):
        n = 2 if size % 2 == 0 else 3
        n2 = PATTERN_SIZE[n]
        new_size = (size // n) * n2
        new_image = [[None] * new_size for k in range(new_size)]
        for r0 in range(size // n):
            for c0 in range(size // n):
                key = next(pattern_to_keys(image, r0 * n, c0 * n, n))
                apply_pattern(new_image, r0 * n2, c0 * n2, n2, patterns[n][key])
        
        image = new_image
        size = new_size
    
    print(sum(sum(c == '#' for c in row) for row in image))
