#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from math import sqrt
    
    n = int(sys.stdin.read().strip())
    k = int(sqrt(n - 1) + 1) // 2
    m = n - (2 * k + 1)**2
    
    offset = abs(m % (2*k) - k)
    
    print(offset + k)
