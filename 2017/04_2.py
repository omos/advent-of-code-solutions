#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    from collections import Counter
    
    valid = 0
    for line in sys.stdin:
        words = line.strip().split(' ')
        if len(set(''.join(sorted(word)) for word in words)) == len(words):
            valid += 1
    
    print(valid)
