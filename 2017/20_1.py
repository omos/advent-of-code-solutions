#!/usr/bin/env python3

def less_than(ref, test):
    for i in range(len(ref)):
        if test[i] < ref[i]:
            return True
        elif test[i] > ref[i]:
            return False
    return False

if __name__ == '__main__':
    import sys
    
    best_value = None
    best = None
    
    i = 0
    for line in sys.stdin:
        p, v, a = (list(map(int, vec.split('=')[1].strip('<>').split(','))) for vec in line.strip().split(', '))
        
        value = [sum(abs(x) for x in v) for v in (a, v, p)]
        if best_value == None or less_than(best_value, value):
            best_value = value
            best = i
        
        i += 1
    
    print(best)
