#!/usr/bin/env python3

DELTAS = {
        'n':  ( 0,  1),
        's':  ( 0, -1),
        'ne': ( 1,  0),
        'sw': (-1,  0),
        'nw': (-1,  1),
        'se': ( 1, -1),
    }

if __name__ == '__main__':
    import sys
    
    dirs = sys.stdin.readline().strip().split(',')
    
    x, y = 0, 0
    for d in dirs:
        dx, dy = DELTAS[d]
        x += dx
        y += dy
    
    dist = max(abs(x), abs(y), abs(x + y))
    print(dist)
