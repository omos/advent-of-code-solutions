#!/usr/bin/env python3

from itertools import *

if __name__ == '__main__':
    import sys
    
    checksum = 0
    for line in sys.stdin:
        row = list(map(int, line.split()))
        checksum += sum(x // y + y // x for x, y in combinations(row, 2) if x % y == 0 or y % x == 0)
    print(checksum)
