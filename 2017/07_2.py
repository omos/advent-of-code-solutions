#!/usr/bin/env python3


if __name__ == '__main__':
    import sys
    from collections import defaultdict
    
    nodes = set()
    inner = set()
    
    weights = dict()
    childrens = dict()
    
    for line in sys.stdin:
        sides = line.strip().split(' -> ')
        children = [] if len(sides) < 2 else sides[1].split(', ')
        name, weight = sides[0].split(' ')
        weight = int(weight.strip('()'))
        
        nodes.add(name)
        inner.update(children)
        
        weights[name] = weight
        childrens[name] = children
    
    root = (nodes - inner).pop()
    
    total_weights = dict()
    
    def calculate_total_weights(node):
        weight = weights[node]
        for child in childrens[node]:
            weight += calculate_total_weights(child)
        total_weights[node] = weight
        return weight
    
    calculate_total_weights(root)
    
    def find_bad_program(node):
        child_weights = defaultdict(list)
        for child in childrens[node]:
            bad = find_bad_program(child)
            if bad != None:
                return bad
            child_weights[total_weights[child]].append(child)
        if len(child_weights) <= 1:
            return None
        for w in child_weights:
            nodes = child_weights[w]
            if len(nodes) == 1:
                return weights[nodes[0]] + sum(w for w in child_weights) - 2 * w
        assert False
    
    print(find_bad_program(root))
