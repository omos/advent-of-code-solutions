#!/usr/bin/env python3

a = 1

if a != 0:
    start = 106700
    end = 123700
else:
    start = 67
    end = 67

counter = 0
for b in range(start, end + 1, 17):
    f = 1
    for d in range(2, b):
        if b % d == 0:
            f = 0
            break
    
    if f == 0:
        counter += 1

print(counter)
