#!/usr/bin/env python3

def run_generator(start, factor):
    val = start
    while True:
        val *= factor
        val %= 0x7FFFFFFF
        yield val


if __name__ == '__main__':
    import sys
    
    factor_a = 16807
    factor_b = 48271
    
    start_a = int(sys.stdin.readline().strip().split(' ')[-1])
    start_b = int(sys.stdin.readline().strip().split(' ')[-1])
    
    a = run_generator(start_a, factor_a)
    b = run_generator(start_b, factor_b)
    
    count = 0
    for i in range(40*1000*1000):
        count += (next(a) & 0xFFFF) == (next(b) & 0xFFFF)
    
    print(count)
