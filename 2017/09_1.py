#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    score = 0
    for line in sys.stdin:
        level = 0
        cancel = False
        garbage = False
        for c in line.strip():
            if cancel:
                cancel = False
            elif c == '!':
                cancel = True
            elif garbage:
                garbage = c != '>'
            elif c == '<':
                garbage = True
            elif c == '{':
                level += 1
                score += level
            elif c == '}':
                level -= 1
    print(score)
