#!/usr/bin/env python3

DATA_LEN = 256

if __name__ == '__main__':
    import sys
    
    lengths = list(map(int, sys.stdin.readline().strip().split(',')))
    
    data = list(range(DATA_LEN))
    pos = 0
    skip_size = 0
    for length in lengths:
        for i in range(length // 2):
            a = (pos + i) % DATA_LEN
            b = (pos + length - i - 1) % DATA_LEN
            data[a], data[b] = data[b], data[a]
        pos += length + skip_size
        skip_size += 1
    
    print(data[0] * data[1])
