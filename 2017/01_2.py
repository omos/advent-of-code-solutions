#!/usr/bin/env python3

if __name__ == '__main__':
    import sys
    
    digits = sys.stdin.read().strip()
    total = 0
    n = len(digits)
    for i in range(n):
        if digits[i] == digits[(i + n // 2) % n]:
            total += int(digits[i])
    print(total)
