#!/usr/bin/env python3

LETTER_RANGE = range(ord('A'), ord('Z') + 1)

def in_range(r, c, grid):
    return 0 <= r and r < len(grid) and 0 <= c and c < len(grid[0])

if __name__ == '__main__':
    import sys
    
    grid = [line.strip('\n') for line in sys.stdin]
    
    letters = ''
    
    r = 1
    c = grid[0].index('|')
    
    dr = 1
    dc = 0
    while in_range(r, c, grid):
        ch = grid[r][c]
        if ord(ch) in LETTER_RANGE:
            letters += ch
        elif ch == '+':
            for ndr, ndc in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
                if (ndr, ndc) == (-dr, -dc):
                    continue
                if not in_range(r + ndr, c + ndc, grid):
                    continue
                nch = grid[r + ndr][c + ndc]
                if nch in '-|' or ord(nch) in LETTER_RANGE:
                    dr, dc = ndr, ndc
                    break
        elif ch == ' ':
            break
        r += dr
        c += dc
    
    print(letters)
